
REGISTRY := registry.gitlab.com/rode0day/fuzzer-testing

.PHONY: afl_i386 afl

runner/grcov:
	wget -qO- 'https://github.com/mozilla/grcov/releases/download/v0.6.1/grcov-linux-x86_64.tar.bz2' | tar -C runner  -xj

base_i386:
	docker build --tag "${REGISTRY}/i386/base:16.04" \
		--file base/i386/Dockerfile base

builder_i386:
	docker build --tag "${REGISTRY}/i386/builder:16.04" \
		--file builder/i386/Dockerfile builder

afl_i386:
	docker build  --build-arg "RUNNER_IMAGE=${REGISTRY}/i386/afl:16.04" \
		--tag "${REGISTRY}/i386/afl_runnner:16.04" \
		--file targets/Dockerfile targets

afl:
	docker build  --build-arg "RUNNER_IMAGE=${REGISTRY}/afl:16.04" \
		--tag "${REGISTRY}/afl_runnner:16.04" \
		--file targets/Dockerfile targets
