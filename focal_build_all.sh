#!/bin/bash -e

cp focal.env .env

git apply --check focal_diff.patch
if [ $? -eq 0 ]; then
    git apply focal_diff.patch
else
    echo "[*] Failure to apply focal_diff.patch"
    exit 1
fi

export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

if [ "$1" = "base" ]; then
    docker-compose build --parallel --no-cache base || exit 1
    docker-compose build --parallel --no-cache builder || exit 1
    git apply --reverse focal_diff.patch
    exit 0
elif [ $# -gt 0 ]; then
    echo "$@"
    docker-compose build ${1}_builder || echo NO builder
    docker-compose build $@ || exit 1
    docker-compose build --no-cache ${1}_runner || exit 1
    docker-compose push ${1}_runner
    git apply --reverse focal_diff.patch
    exit 0
fi

docker-compose build base || exit 1
docker-compose build builder || exit 1

# build i386 separately without buildkit
unset COMPOSE_DOCKER_CLI_BUILD
unset DOCKER_BUILDKIT
docker-compose build base_i386 || exit 1
docker-compose build builder_i386 || exit 1
docker-compose build --parallel afl_i386 aflpp_i386 honggfuzz_i386 ankou || exit 1
docker-compose push afl_i386 aflpp_i386 honggfuzz_i386
docker-compose build --parallel verify_i386 symcc_i386 || exit 1
docker-compose push verify_i386
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

# removed afl(qemu), eclipser(qemu)
docker-compose build --parallel afl-rb wafl aflpp honggfuzz || exit 1
docker-compose build --parallel afl || exit 1
docker-compose build --parallel angora_builder parmesan_builder || exit 1
docker-compose build --parallel angora parmesan ankou verify symcc || exit 1


# build the runners
docker-compose build --parallel --no-cache afl_runner aflpp_runner || exit 1
docker-compose build --parallel --no-cache honggfuzz_runner || exit 1
docker-compose build --parallel --no-cache angora_runner ankou_runner parmesan_runner || exit 1
docker-compose build --parallel --no-cache test_runner verify_runner symcc_runner || exit 1
# these are tagged versions of built runners
docker-compose build aflrb_runner wafl_runner

unset COMPOSE_DOCKER_CLI_BUILD
unset DOCKER_BUILDKIT
docker-compose build --parallel --no-cache afl_i386_runner aflpp_i386_runner honggfuzz_i386_runner verify_i386_runner

# push to the repository
docker-compose push afl_runner aflpp_runner afl_i386_runner aflpp_i386_runner aflrb_runner \
                    honggfuzz_runner honggfuzz_i386_runner angora_runner ankou_runner \
                    wafl_runner test_runner verify_runner verify_i386_runner \
                    parmesan_runner symcc_runner


git apply --reverse focal_diff.patch
