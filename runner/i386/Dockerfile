ARG RUNNER_IMAGE
FROM $RUNNER_IMAGE 
LABEL maintainer="jmb@iseclab.org" \
      type="runner"

USER root

ENV AFL_SKIP_CPUFREQ=1 \
    QEMU_RESERVED_VA=0xf700000 \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    SETUPTOOLS_USE_DISTUTILS=stdlib

COPY requirements.txt /root/

RUN python3 -m pip install --no-cache-dir -r /root/requirements.txt && \
    git clone --depth=1 -b rode0day https://gitlab.com/wideglide/afl-utils.git /tmp/afl-utils && \
    cd /tmp/afl-utils && \
    cp ansi2html.sh /usr/local/bin/ && \
    CC=gcc python3 setup.py install && \
    cd / && rm -rf /tmp/afl-utils && \
    ln -fL /usr/bin/python3 /usr/local/bin/python && \
    ldconfig

COPY afl-make-dictionary.sh \
     make-gcov-src.sh \
     job_control.py \
     afl-mon-cov_32 \
     afl-mon-cov_64 \
     afl-fast-cov_32 \
     afl-fast-cov_64 \
     grcov \
     start-test.sh \
     start-afl.sh \
     start-qsym.sh \
     start-honggfuzz.sh \
     start-eclipser.sh \
     start-ankou.sh \
     start-angora.sh \
     start-symcc.sh \
     monitor.py /usr/local/bin/

COPY start_fuzzing /

WORKDIR /data
USER fuzz

ENTRYPOINT [ "/start_fuzzing" ]
