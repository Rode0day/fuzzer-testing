#!/bin/bash

[ -d dict ] && rm -rf dict
mkdir -p dict

bin_file=$(python -c "import sys, json; print(json.load(open(sys.argv[1]))['target'])" ${1})

objdump -d "${bin_file}" | grep -Eo '\$0x[0-9a-f]+' | cut -c 2- | sort -u | python3 -c "import sys, struct; print('\"'+'\"\n\"'.join(''.join('\\\x%02x' % b for b in struct.pack('<I' if len(l) <= 11 else '<Q', int(l,0))) for l in sys.stdin.readlines())+'\"' )" > dict/consts.txt

i=0
strings "${bin_file}" | sed 's/"/\\\\"/g' |  while read line; do echo "\"$line\"" >> dict/strings.txt ; i=$[ $i + 1 ] ; done
sed -i 's/\t/\\x09/g' dict/strings.txt
sed -i -E '/^.{96}/d' dict/strings.txt
cat dict/consts.txt dict/strings.txt > dict/dictionary.txt
