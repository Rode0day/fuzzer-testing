#!/bin/bash

# variable defaults
FZ=intriguer

# intriguer_path = {INTRIGUER_ROOT}/bin/run_intriguer.py
# intruiguer_timeout = 90
# fname = queue_cur->fname
# python {intriguer_path} -s 1 -t {timeout} -t {fname} -o {temp_dir} -- {intriguer_cmd}

start_intriguer() {
    afl-multicore -c ${FZ}_job.json -s $DELAY start 1
    if [[ $N -gt 1 ]]; then
        sed -i 's/master_instances":.*,/master_instances": 0,/' afl_job.json
        sed -i 's/dict/_dict/' afl_job.json
        local AFL_N="$(( N - 1 ))"
        sleep 2s
        afl-multicore -c afl_job.json -s $DELAY add $AFL_N
    fi
}

stop_intriguer(){
    [ -e .jobvars ] && source .jobvars
    TGT=${TGT:-$1}
    afl-multikill -c ${FZ}_job.json
}

check_intriguer(){
   afl-stats -c ${FZ}_job.json -r
}

process_results() {
    afl-stats -c ${FZ}_job.json -d stats.db -r
}

