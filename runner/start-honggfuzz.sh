#!/bin/bash

FZ=honggfuzz
FZs=hf

# CC=hfuzz-clang CXX=hfuzz-clang++ CFLAGS="-fsanitize=address" ./configure
# -- compile
# hfuzz-clang -I ./jpeg-9c/ /examples/libjpeg/persistent-jpeg.c -o persistent.jpeg9.address jpeg-9c/.libs/libjpeg.a  -fsanitize=address
# honggfuzz -i initial_corpus --rlimit_rss 2048 -- ./persistent.jpeg9.address
# QEMU mode
# honggfuzz -i input_dir -- <honggfuzz_dir>/qemu_mode/honggfuzz-qemu/x86_64-linux-user/qemu-x86_64 /usr/bin/djpeg ___FILE___
# AFL "like"
# honggfuzz -i inputs -o outputs 

start_honggfuzz() {
    /usr/local/bin/job_control.py --start honggfuzz ${FZ}_job.json -N $N
    source .jobvars
    OUTPUTS="${OUTPUTS:-outputs}"
    SYNCDIR="$OUTPUTS/${TGT}_${FZ}"
    Q_DIR="$SYNCDIR/queue"
    C_DIR="$SYNCDIR/crashes"
}

stop_honggfuzz() {
    [ -e .jobvars ] && source .jobvars
    TGT=${TGT:-$1}
    local outdir=${OUTPUTS:-outputs}
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} | tee -a ${outdir}/final_console.log
    tmux send-keys -t ${TGT}:${TGT}_${FZ} C-c
}

check_honggfuzz() {
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} -E 12 > $OUTPUTS/console.log
    [ -e .jobvars ] && source .jobvars
    if [[ -n $FUZZER_PID ]]; then
        echo -ne "[*] HONGGFUZZ "
        ps -o pid,ppid,pgid,start,time,stat,args -p $FUZZER_PID
    fi
}  

process_results() {
    local LOGDIR=${SYNCDIR:-outputs}
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} > $LOGDIR/console.log
    parse_console $LOGDIR/console.log > $LOGDIR/fuzzer_stats
    afl-stats -c ${FZ}_job.json -d stats.db -r
}

parse_console() {
    [ -e $1 ]  || return
    head -n12 $1 | \
    sed '/----/d; /Mode/d; s/\x1B\[\([0-9;]\+\)m//g; s/,//g; s/\/sec//; s/\]//g; s/\[//g; /^\s*$/d;' | parse
}

parse() {
read _ _ iterations _
read _ _ cmdline
read _ _ threads _ cpus _
read _ _ speed _ avg 
read _ _ crashes _ c_unique _
read _ _ timeouts tmout _
read _ _ _ queue_sz _ max_size _ _ init_sz _
read _
read _ _ _ edge cov_pct _ cov_pc _ cov cmp

MODE=default
if [[ $cmdline == *'qemu'* ]]; then
    MODE=qemu
fi

if [ -z "$START_TIME" ]; then
    echo "[-] START_TIME not exported"
    START_TIME="$(stat -c "%Y" .jobvars)"
fi

local last_path="$(cd $Q_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local last_crash="$(cd $C_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local banner="${TGT}_${FZ}"
local queue=${queue_sz:-1}
local init=${init_sz:-1}

echo "start_time        : $START_TIME
last_update       : $(date +"%s")
fuzzer_pid        : ${FUZZER_PID:-0}
cycles_done       : 0
execs_done        : ${iterations//,/}
execs_per_sec     : ${avg}
paths_total       : ${queue}
paths_favored     : 0
paths_found       : $(( $queue - $init ))
paths_imported    : 0
max_depth         : 0
cur_path          : 0
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : ${cov_pct}
unique_crashes    : ${c_unique}
unique_hangs      : ${timeouts}
last_path         : $last_path
last_crash        : $last_crash
last_hang         : 0
execs_since_crash : 0
exec_timeout      : $tmout
afl_banner        : ${banner}
afl_version       : v2.2
target_mode       : $MODE
command_line      : $cmdline"
}

