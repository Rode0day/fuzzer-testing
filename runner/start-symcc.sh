#!/bin/bash

FZ=symcc

start_symcc() {

  local RUN_SYMCC="/usr/local/bin/symcc_fuzzing_helper"
  local SYNC_DIR="${OUTPUTS:-outputs}"
  local AFL_N="$(( $N / 2 ))"
  sed -i 's/master_instances":.*,/master_instances": 0,/' afl_job.json

  afl-multicore -c afl_job.json -s $DELAY $ACTION $AFL_N

  sleep 2m
  
  [ ! -e $RUN_SYMCC ] && exit 1
  [ ! -d $SYNC_DIR ] && exit 1

  /usr/local/bin/job_control.py --start symcc ${FZ}_job.json
  
}

stop_symcc() {
    TGT=${TGT:-$1}
    tmux send-keys -t ${TGT}:${TGT}_Q C-c
    afl-multikill -c ${FZ}_job.json
}

check_symcc() {
   pgrep -a symcc_fuzzing_helper | grep $TGT
}

process_results() {
    afl-stats -c ${FZ}_job.json -d stats.db -r
}

