#!/bin/bash

FZ=qsym

start_qsym() {

  local RUN_QSYM_AFL=/usr/local/bin/run_qsym_afl
  local SYNC_DIR=outputs
  local CFG=""
  local AFL_N="$(( $N / 2 ))"
  sed -i 's/master_instances":.*,/master_instances": 0,/' ${FZ}_job.json

  afl-multicore -c ${FZ}_job.json -s $DELAY $ACTION $AFL_N

  sleep 2m
  
  [ ! -e $RUN_QSYM_AFL ] && exit 1
  [ ! -d $SYNC_DIR ] && exit 1
  
  if [ -e qsym_job.json ]; then 
      CFG="-c qsym_job.json"
  fi
  
  echo tmux new-window -ad -t $TGT -n "${TGT}Q01" $RUN_QSYM_AFL -a "${TGT}001" -n "${TGT}_Q01" -o $SYNC_DIR $CFG
  for i in {0..9}; do
      if [ -d "${SYNC_DIR}/${TGT}00$i" ]; then
          tmux new-window -ad -t $TGT -n "${TGT}Q0$i" $RUN_QSYM_AFL -a "${TGT}00$i" -n "${TGT}_Q0$i" -o $SYNC_DIR $CFG
          sleep 2s
      fi
  done
}

stop_qsym() {
    TGT=${TGT:-$1}
    tmux send-keys -t ${TGT}:${TGT}Q C-c
    afl-multikill -c ${FZ}_job.json
}

check_qsym() {
   pgrep -a qsym | grep $TGT
}

process_results() {
    afl-stats -c ${FZ}_job.json -d stats.db -r
}

