#!/bin/bash

FZ=ankou
FZs=ak
MAXTIME=${MAXTIME:-86400}

#  Ankou -app ./lava-afl-gcc/bin/sqlite -dict dict/consts.txt -dur 1h -i inputs -o outputs/ankou_sqliteB -threads 2

start_ankou() {
    /usr/local/bin/job_control.py --start ankou ${FZ}_job.json -N $N --time-limit $MAXTIME
    source .jobvars
    export OUTPUTS="${OUTPUTS:-outputs}"
    export SYNCDIR="$OUTPUTS/${TGT}_${FZ}"
    export Q_DIR="$SYNCDIR/queue"
    export C_DIR="$SYNCDIR/crashes"
    
    sleep 1s
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} -E 14 > ${OUTPUTS}/initial_console.log
}

stop_ankou() {
    [ -e .jobvars ] && source .jobvars
    TGT=${TGT:-$1}
    local outdir=${OUTPUTS:-outputs}
    tmux send-keys -t ${TGT}:${TGT}_${FZ} C-c
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} | tee ${outdir}/final_console.log
}

check_ankou() {
    [ -e .jobvars ] && source .jobvars
    if [[ -n $FUZZER_PID ]]; then
        echo -ne "[*] ANKOU "
        ps -o pid,ppid,pgid,start,time,stat,args -p $FUZZER_PID
    fi
}

process_results() {
    local LOGDIR=${SYNCDIR:-outputs}
    local seed_mgr=${OUTPUTS:-outputs}/ankou/status*/seed_manager.csv
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} -E 18 > ${LOGDIR}/console.log
#   /usr/local/bin/write_stats_file.py $seed_mgr ${LOGDIR}/console.log > ${LOGDIR}/fuzzer_stats
    parse_console ${LOGDIR}/console.log > ${LOGDIR}/fuzzer_stats
    afl-stats -c ${FZ}_job.json -d stats.db -r
}


# Target: sqlite
# round number: 878 (14m38s)
# len(seedPts) = 1222 - throughput: 602
# total unique crashes: 0 - hangs: 4
# #edges: 7.06e+03 (10.77%) - rate: 410
# Basis dim: 32 - explained var: 70.43% (tot=4.3e+03) - dFloor: 33.3
# general variance: 2.32e+48 - PCA-entropy: 13.9

parse_console() {
    [ -e $1 ] || return
    head -n12 $1 | \
    sed '/^\s*$/d; s/)//g; s/(//g; s/ - / /g; s/=//g' | write_stats_file
}


write_stats_file() {
read _ tgt_bin
read _ _ rounds runtime
read _ len_seeds _ throughput
read _ _ _ n_crashes _ hangs
read _ n_edges cov_pct _ rate
read _
read _

local last_path="$(cd $Q_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local last_crash="$(cd $C_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local queue="$(cd $Q_DIR; ls | wc -l )"
local crashes="$(cd $C_DIR; ls | wc -l )"
local init="$(cd $INPUTS; ls | wc -l )"
local banner="${TGT}_${FZ}"
local cmdline="${BINARY} ${CMDLINE}"
local seed_mgr=${OUTPUTS:-outputs}/ankou/status*/seed_manager.csv

IFS=, read mem seed_n file_size_mean tr_size_mean exec_time_mean execN etime <<< "$(tail -n1 $seed_mgr )"

echo "start_time        : $START_TIME
last_update       : $(date +"%s")
fuzzer_pid        : ${FUZZER_PID:-0}
cycles_done       : ${rounds:-0}
execs_done        : ${execN:-0}
execs_per_sec     : ${throughput:-0}
paths_total       : ${queue:-0}
paths_favored     : ${n_queue:-0}
paths_found       : $(( $queue - $init ))
paths_imported    : 0
max_depth         : 0
cur_path          : ${seed_n:-0}
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : ${cov_pct:-0.0}
unique_crashes    : ${crashes:-0}
unique_hangs      : ${hangs:-0}
last_path         : $last_path
last_crash        : $last_crash
last_hang         : 0
execs_since_crash : 0
exec_timeout      : ${tmout:-0}
afl_banner        : ${banner}
afl_version       : v1.0
target_mode       : ankou
command_line      : $cmdline" 
}
