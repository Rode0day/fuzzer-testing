#!/bin/bash

FZ=eclipser
FZs=ec
MAXLEN=${MAXLEN:-2097152}
MAXTIME=${MAXTIME:-86400}


# dotnet ../Eclipser/build/Eclipser.dll fuzz -p <target_bin> -t <time_limit> -v 0 -i seeds \
#  --src file --maxfilelen 1048576 --initarg "<cmdline>" --fixfilepath $5 \
#  --nspawn <10> --nsolve <600> 

# dotnet /opt/Eclipser/Eclipser.dll fuzz -p ./built/bin/jpeg -t 86400 -i inputs -o outputs --src file --maxfilelen 1048576 --initarg ".input" -f .input --nspawn 10 --nsolve 600 --architecture x86 -v 0
# dotnet /opt/Eclipser/Eclipser.dll decode -i outputs/testcase -o outputs/decoded
# mv outputs/decoded/decoded_files outputs/ec/queue

start_eclipser() {
    if [[ $N -gt 1 ]]; then
        # launch AFL instance
        sed -i 's/master_instances":.*,/master_instances": 0,/' afl_job.json
        sed -i 's/dict/_dict/' afl_job.json
        local AFL_N="$(( N - 1 ))"
        afl-multicore -c afl_job.json -s $DELAY start $AFL_N
    fi

    # launch Eclipser
    /usr/local/bin/job_control.py --start eclipser ${FZ}_job.json -N 1 --time-limit $MAXTIME
    source .jobvars

    if [[ $CMDLINE =~ "@@" ]]; then
        DECODED="decoded_files"
    else
        DECODED="decoded_stdins"
    fi

    OUTPUTS="${OUTPUTS:-outputs}"
    SYNCDIR="$OUTPUTS/${TGT}_${FZ}"
    Q_DIR="$SYNCDIR/queue"
    C_DIR="$SYNCDIR/crashes"
}

stop_eclipser() {
    [ -e .jobvars ] && source .jobvars
    TGT=${TGT:-$1}
    afl-multikill -c afl_job.json
    local outdir=${OUTPUTS:-outputs}
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} | tee ${outdir}/final_console.log
    tmux send-keys -t ${TGT}:${TGT}_${FZ} C-c
    kill -SIGTERM $DECODE_PID
}

eclipser_decode_files() {
    dotnet /opt/Eclipser/Eclipser.dll decode -i ${OUTPUTS}/testcase -o ${OUTPUTS}/queue
    rsync --temp-dir=/tmp -W --size-only ${OUTPUTS}/queue/${DECODED}/ ${Q_DIR}/
    dotnet /opt/Eclipser/Eclipser.dll decode -i ${OUTPUTS}/crash -o ${OUTPUTS}/crashes
    rsync --temp-dir=/tmp -W --size-only ${OUTPUTS}/crashes/${DECODED}/ ${C_DIR}/
}

check_eclipser() {
    [ -e .jobvars ] && source .jobvars
    if [[ -n $FUZZER_PID ]]; then
        echo -ne "[*] ECLIPSER "
        ps -o pid,ppid,pgid,start,time,stat,args -p $FUZZER_PID
    fi
}

process_results() {
    local LOGDIR=${SYNCDIR:-outputs}
    write_stats_file > $LOGDIR/fuzzer_stats
    afl-stats -c ${FZ}_job.json -d stats.db -r
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} -E 28 > ${LOGDIR}/console.log
}

write_stats_file() {
local last_path="$(cd $Q_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local last_crash="$(cd $C_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local queue="$(cd $Q_DIR; ls | wc -l )"
local crashes="$(cd $C_DIR; ls | wc -l )"
local init="$(cd $INPUTS; ls | wc -l )"
local banner="${TGT}_${FZ}"
local cmdline="${BINARY} ${CMDLINE}"

echo "start_time        : $START_TIME
last_update       : $(date +"%s")
fuzzer_pid        : ${FUZZER_PID:-0}
cycles_done       : 0
execs_done        : ${iterations:-0}
execs_per_sec     : ${avg:-0}
paths_total       : ${queue:-0}
paths_favored     : 0
paths_found       : $(( $queue - $init ))
paths_imported    : 0
max_depth         : 0
cur_path          : 0
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : ${cov_pct:-0.0}
unique_crashes    : ${c_unique:-0}
unique_hangs      : ${timeouts:-0}
last_path         : $last_path
last_crash        : $last_crash
last_hang         : 0
execs_since_crash : 0
exec_timeout      : ${tmout:-0}
afl_banner        : ${banner}
afl_version       : v1.0
target_mode       : qemu
command_line      : $cmdline" 
}
