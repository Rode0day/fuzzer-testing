#!/bin/bash

# variable defaults
FZ=${FZ:-afl}

start_afl() {
    afl-multicore -c ${FZ}_job.json -s $DELAY $ACTION $N
}

stop_afl(){
    TGT=${TGT:-$1}
    afl-multikill -c ${FZ}_job.json
}

check_afl(){
   afl-stats -c ${FZ}_job.json -r
}

process_results() {
    afl-stats -c ${FZ}_job.json -d stats.db -r
}

