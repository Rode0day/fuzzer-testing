#!/usr/bin/env python3

# Given a configuration file, watch an output directory for coverage-improving inputs
# whenever a file shows up, mark the time (relative from the first) and store all the new
# blocks covered in the database

import csv
import glob
import hashlib
import json
import logging
import multiprocessing
import os
import re
import resource
import shlex
import shutil
import signal
import struct
import sys
import tempfile
import time
import peewee as pw

from datetime import datetime
from inotify_simple import INotify, flags

from subprocess import Popen, PIPE, SubprocessError
from os import setsid, killpg, getpgid  # noqa

# Logging setup
logger = multiprocessing.log_to_stderr(logging.INFO)
formatter = logging.Formatter('%(levelname)-7s | %(asctime)-23s | %(processName)-8s | %(message)s')
logger.handlers[0].setFormatter(formatter)

# Globals
current_target = None
current_experiment = None
config_arch = 32  # default to 32-bit, check binary for ELF header
preload = None  # If we need to LD_PRELOAD. Untested
trace_logdir = tempfile.mkdtemp(prefix="cov_")
db_proxy = pw.DatabaseProxy()
QEMU_PATH = '/usr/local/bin/afl-mon-cov'

if len(sys.argv) < 3 or not os.path.isfile(sys.argv[2]):
    raise RuntimeError("Usage {} [experiment_name] [job config file].json".format(sys.argv[0]))

#############################################
#############################################
#           Parse configuration             #
#  We do this here so we connect to the DB  #
#  before initializing all our ORM classes  #
#############################################
with open(sys.argv[2]) as f:
    config = json.load(f)

# by convention, drcov_target is an uninstrumented version of the target meant for gathering coverage
tgt_binary = config['drcov_target']
asan_binary = config.get('asan_target', tgt_binary)

if not os.path.isfile(asan_binary):
    asan_binary = tgt_binary
module = os.path.basename(tgt_binary)

# set the 'marker' for detecting reached/triggered bugs
bug_marker = config.get('bug_marker', 'LAVALOG:').encode()

# Detect custom target list
target_blocks = config.get('target_blocks')

# SETUP PATHS - Constant inside containers but change for debugging on host
fuzz_root = os.path.abspath(config['output'])

# event to control graceful shutdown
stop_soon = multiprocessing.Event()

# regex for bug_id
digits_re = re.compile(r'\D*(\d+).*')


class BaseModel(pw.Model):
    class Meta:
        database = db_proxy


class Target(BaseModel):  # A target we're fuzzing: unique shahash
    name = pw.CharField()
    arch = pw.IntegerField()  # 32 or 64
    shahash = pw.CharField(unique=True)  # sha1 hash of target binary
    # <- experiments
    # <- blocks
    # <- targets

    def __str__(self):
        return "Target {} (x{})".format(self.name, self.arch)


class Experiment(BaseModel):
    # An experiment is some configuration of a fuzzer (described soley through experiment name), and a target
    # Experiment names must be unique
    name = pw.CharField(unique=True)
    target = pw.ForeignKeyField(Target, backref='experiments')
    # <- blocks
    # <- edges
    # <- inputs
    # <- bugs
    # <- plotdata

    def __str__(self):
        return "Experiment {}".format(self.name)


class PlotData(BaseModel):
    # each row is a log of (blocks, edges, bugs)
    reltime = pw.IntegerField()     # timestamp from start
    blocks = pw.IntegerField()      # number of observed blocks
    edges = pw.IntegerField()       # number of observed edges
    bugs = pw.IntegerField()        # number of observed bugs
    crashes = pw.IntegerField()     # number of observed crashing inputs
    experiment = pw.ForeignKeyField(Experiment, backref='plotdata', on_delete='CASCADE')

    def __str__(self):
        return "snapshot: {} blocks, {} edges, {} bugs at reltime {}".format(
               self.blocks, self.edges, self.bugs, self.reltime)


class TestCase(BaseModel):
    # A test case is an input for a target ( could be a crash or a queue file)
    reltime = pw.IntegerField()     # timestamp from first input
    filename = pw.CharField()       # filename
    blocks = pw.IntegerField()      # unique blocks for this input
    edges = pw.IntegerField()       # unique edges for this input
    new_blocks = pw.IntegerField()  # new blocks for this input
    new_edges = pw.IntegerField()   # new edges for this input
    returncode = pw.IntegerField()  # execution return code
    size = pw.IntegerField()        # testcase size when reported
    experiment = pw.ForeignKeyField(Experiment, backref='inputs', on_delete='CASCADE')
    # <- found_blocks
    # <- found_edges
    # <- bug

    def __str__(self):
        return "Input: {}  with ({}, {}) new (bbs, edges) and ({}, {}) total.".format(
                self.filename, self.new_blocks, self.new_edges, self.blocks, self.edges)


class LavaBug(BaseModel):
    bug_id = pw.IntegerField()   # assigned lava bug id
    src_line = pw.CharField()    # source file and line number
    target = pw.ForeignKeyField(Target, backref='bugs', on_delete='CASCADE')

    class Meta:
        indexes = (
            (('bug_id', 'target'), True),
        )

    def __str__(self):
        return "lava bug: {} at {} in {}".format(self.bug_id, self.src_line, self.target)


class FoundBug(BaseModel):
    reltime = pw.IntegerField()  # timestamp from first input
    returncode = pw.IntegerField()  # return code from execution
    bug = pw.ForeignKeyField(LavaBug, backref='discoveries', on_delete='CASCADE')
    testcase = pw.ForeignKeyField(TestCase, backref='bug', on_delete='CASCADE')
    experiment = pw.ForeignKeyField(Experiment, backref='bugs', on_delete='CASCADE')

    def __str__(self):
        return "bug: {} found after {} s".format(self.block, self.reltime)


class Block(BaseModel):
    address = pw.BigIntegerField()  # basic block start
    size = pw.IntegerField()        # basic block size
    target = pw.ForeignKeyField(Target, backref='blocks', on_delete='CASCADE')
    # <- discoveries

    class Meta:
        indexes = (
            (('address', 'target'), True),
        )

    def __str__(self):
        return "Block at 0x{:x} len {} in {}".format(self.address, self.size, self.target)


class FoundBlock(BaseModel):
    reltime = pw.IntegerField()  # timestamp from first input
    block = pw.ForeignKeyField(Block, backref='discoveries', on_delete='CASCADE')
    testcase = pw.ForeignKeyField(TestCase, backref='found_blocks', on_delete='CASCADE')
    experiment = pw.ForeignKeyField(Experiment, backref='blocks', on_delete='CASCADE')

    def __str__(self):
        return "{} found after {} s".format(self.block, self.reltime)


class Edge(BaseModel):
    start = pw.BigIntegerField()  # Source block address of edge
    end = pw.BigIntegerField()    # Destination block address of edge
    target = pw.ForeignKeyField(Target, backref='edges', on_delete='CASCADE')
    # <- discoveries

    class Meta:
        indexes = (
            (('start', 'end', 'target'), True),
        )

    def __str__(self):
        return "Edge between 0x{:x}-0x{:x} in {}".format(self.start, self.end, self.target)


class FoundEdge(BaseModel):
    reltime = pw.IntegerField()  # timestamp from first input
    edge = pw.ForeignKeyField(Edge, backref='discoveries', on_delete='CASCADE')
    testcase = pw.ForeignKeyField(TestCase, backref='found_edges', on_delete='CASCADE')
    experiment = pw.ForeignKeyField(Experiment, backref='edges', on_delete='CASCADE')

    def __str__(self):
        return "{} found after {} s".format(self.edge, self.reltime)


class Watcher(multiprocessing.Process):

    def __init__(self, out_q, root_dir):
        multiprocessing.Process.__init__(self)
        self.inotify = INotify()
        self.root_path = os.path.abspath(root_dir)
        self.q_wd = dict()
        self.all_dirs = set()
        self.fuzzers = list()
        self.valid_qdirs = set(['queue', 'crashes'])
        self.out_q = out_q
        self.c = 0
        self.last_file = None
        self.created = set()

    def run(self):
        self.start_time = datetime.now().timestamp()
        while not stop_soon.is_set():
            self.reltime = round(datetime.now().timestamp() - self.start_time)
            # check for new queue directories within first hour
            if self.reltime < (60 * 60):
                self.maybe_add_fuzzer()
            if len(self.q_wd):
                self.read_events()
            time.sleep(0.8)
        logger.info("Shutting monitor down. Queue size = %d", self.out_q.qsize())
        self.out_q.put(None)
        self.log_file()

    def read_events(self):
        for e in self.inotify.read(timeout=60000):
            if (e[1] & flags.ISDIR):
                continue
            wd_name = (e[0], e[3])
            if (e[1] & flags.CREATE):
                self.created.add(wd_name)
            elif (e[1] & flags.CLOSE_WRITE) and wd_name in self.created:
                self.created.remove(wd_name)
                self.add_file(wd_name)

    def maybe_add_fuzzer(self):
        for f in os.scandir(self.root_path):
            if f.is_dir() and f.path not in self.all_dirs:
                self.all_dirs.add(f.path)
                self.fuzzers.append(f.path)
                logger.info("adding #%d, F:%s ", self.c, f.path)
        for fdir in self.fuzzers:
            if os.path.isdir(fdir):
                self.maybe_add_qdir(fdir)

    def maybe_add_qdir(self, path):
        for f in os.scandir(path):
            if f.is_file() or f.path in self.all_dirs:
                continue
            self.all_dirs.add(f.path)
            if f.name in self.valid_qdirs:
                rpath = os.path.realpath(f.path)
                new_wd = self.inotify.add_watch(rpath, flags.CREATE | flags.CLOSE_WRITE)
                self.q_wd[new_wd] = f.path
                logger.info("adding #%d, wd:%d q:%s ", self.c, new_wd, f.path)
                self.add_files_from_dir(new_wd, f.path)

    def add_files_from_dir(self, wd, path):
        for f in os.scandir(path):
            if f.is_file():
                self.add_file((wd, f.name))

    def add_file(self, wd_name):
        wd, name = wd_name
        file_path = os.path.join(self.q_wd[wd], name)
        self.out_q.put((file_path, self.reltime))
        self.c += 1
        self.last_file = file_path
        if self.c % 100 == 0:
            self.log_file()

    def log_file(self):
        logger.info("adding #%d, Q:%d %s", self.c, self.out_q.qsize(), self.last_file)


def handle_timeout(pid):
    try:
        os.kill(pid, signal.SIGKILL)
    except ProcessLookupError as e:
        print(e, flush=True)


class Forkserver(multiprocessing.Process):
    def __init__(self, in_q, cmds, quiet=True):
        multiprocessing.Process.__init__(self)
        self.ctl_out, self.ctl_in = os.pipe()
        self.st_out, self.st_in = os.pipe()
        self.hide_output = quiet
        self.timeout = 5
        self.in_q = in_q
        self.pre_cmds = cmds

    def child(self):
        FORKSRV_FD = 198  # from AFL config.h
        os.dup2(self.ctl_out, FORKSRV_FD)
        os.dup2(self.st_in, FORKSRV_FD+1)

        if self.hide_output:
            null_fd = os.open('/dev/null', os.O_RDWR)
            os.dup2(null_fd, 1)
            os.dup2(null_fd, 2)
            os.close(null_fd)

        os.dup2(self.in_file.fileno(), 0)
        os.close(self.in_file.fileno())

        os.close(self.ctl_in)
        os.close(self.ctl_out)
        os.close(self.st_in)
        os.close(self.st_out)
        os.setsid()
        mem_limit = 2**30 * 4  # mem limit 4GB
        resource.setrlimit(resource.RLIMIT_CORE, (0, 0))
        resource.setrlimit(resource.RLIMIT_AS, (mem_limit, mem_limit))
        env = os.environ.copy()
        env["TRACE_OUT_DIR"] = trace_logdir
        env["QEMU_LOG"] = "nochain"
        ARGS = shlex.split(self.cmd)
        qemu_path = "{}_{}".format(QEMU_PATH, config_arch)
        os.execve(qemu_path, ["afl-mon-cov"]+ARGS, env)
        print("child failed")

    def parent(self, fork_pid):
        os.close(self.ctl_out)
        os.close(self.st_in)
        status = os.read(self.st_out, 4)
        if len(status) == 4:
            self.observed_blocks = set()  # (Start address, size) of basic blocks
            self.observed_edges = set()   # (start,end) tuples
            self.reached_bugs = set()     # reached bugs (bug id)
            self.triggered_bugs = set()   # triggered bugs (bug id)
            self.targets = set()          # directed mode blocks (addresses)
            self.directed_mode = target_blocks is not None
            self.counter = 0
            self.num_crashes = 0
            self.n0, self.n1 = 0, 1
            return True
        logger.error("Failed to initialize forkserver!")
        return False

    def run(self):
        # Ignore Ctrl-C in this process
        signal.signal(signal.SIGINT, signal.SIG_IGN)

        tfile = ".cov_input{:06d}".format(os.getpid())
        self.in_file = open(tfile, "wb+")
        self.cmd = self.pre_cmds[0].replace('@@', tfile)
        self.asan_cmd = self.pre_cmds[1].replace('@@', tfile)

        fork_pid = os.fork()
        if fork_pid == 0:
            self.child()
            return
        if not self.parent(fork_pid):
            return

        if self.directed_mode:
            self.setup_target_list()

        self.start_time = datetime.now().timestamp()
        logger.info("Starting monitor-execution process.")
        while True:
            if not self.process_testcase():
                break
        logger.info("Shutting down monitor-execution process. in_Q=%d", self.in_q.qsize())
        self.write_stats()

    def process_testcase(self):
        # pop one result from the queue
        testcase = self.in_q.get()
        if testcase is None:
            return False
        result = self.execute_input(testcase)
        tc_id = self.process_traces(result)
        if len(result['bugs']) > 0:
            self.record_bugs(result, tc_id)
        if result['reltime'] > self.n1:
            self.n0, self.n1 = min(self.n1, 300), self.n0 + self.n1
            self.write_stats(result['reltime'])
        return True

    def execute_input(self, testcase):
        # pop one input from the queue
        (filename, reltime) = testcase

        # write testcase to the input file
        self.in_file.truncate(0)
        self.in_file.seek(0)
        with open(filename, 'rb') as fd:
            self.in_file.write(fd.read())
        self.in_file.seek(0)

        # first execute native binary to check for lava bug id and return code
        result = run_target(self.asan_cmd, self.in_file, timeout=2, check_lava=True)
        result.update({'filename': filename, 'reltime': reltime})
        self.in_file.seek(0)

        # execute a second time for cov capture
        if os.write(self.ctl_in, b"\0\0\0\0") != 4:
            logger.error("fork server dead? PID {}".format(os.getpid()))
            return result

        pid = struct.unpack("I", os.read(self.st_out, 4))[0]
        result['pid'] = pid

        signal.signal(signal.SIGALRM, lambda signum, sigfr: handle_timeout(pid))
        signal.setitimer(signal.ITIMER_REAL, self.timeout, 0)
        status = os.read(self.st_out, 4)
        signal.setitimer(signal.ITIMER_REAL, 0, 0)  # disable timer for timeout
        if len(status) != 4:
            logger.error("fork server dead? PID {}".format(os.getpid()))
            return result
        result['status'] = struct.unpack("I", status)[0]
        return result

    def process_traces(self, data):
        filename = data['filename']
        reltime = data['reltime']
        rc = data['returncode']
        edges = set()
        blocks = set()

        # get testcase size
        try:
            size = os.stat(filename).st_size
        except OSError:
            logger.error("testcase not found after execution: %s", filename)
            size = 0

        traces = glob.glob("{}/trace*{:05d}*.qemu".format(trace_logdir, data['pid']))
        if not len(traces):
            logger.error("no log created for %s", filename)
            return record_testcase(reltime, filename, 0, 0, 0, 0, rc, size)
        for trace in traces:
            self.decode_trace(trace, blocks, edges)
            os.remove(trace)
        new_blocks = blocks.difference(self.observed_blocks)
        new_edges = edges.difference(self.observed_edges)

        # Insert test case into database
        testcase = record_testcase(
                reltime, filename, len(blocks), len(edges), len(new_blocks), len(new_edges), rc, size)

        if len(new_blocks) and testcase is not None:
            record_bbs(new_blocks, reltime, testcase)
            self.observed_blocks.update(new_blocks)
            if self.directed_mode:
                self.check_target_list(new_blocks, reltime)

        if len(new_edges) and testcase is not None:
            self.counter += 1
            record_edges(new_edges, reltime, testcase)
            self.observed_edges.update(new_edges)

        # Maybe update crashes counter
        if abs(rc) > 2:
            self.num_crashes += 1

        return testcase

    def decode_trace(self, trfile, blocks, edges):
        with open(trfile, 'r') as fd:
            reader = csv.reader(fd, delimiter=',')
            for row in reader:
                src = int(row[0], 16) if 'ffffffff' not in row[0] else False
                dst = int(row[1], 16) if 'ffffffff' not in row[1] else False
                size = int(row[2], 10) if isinstance(dst, int) else 0  # noqa
                if dst:
                    blocks.add((dst, size))
                if src and dst:
                    edges.add((src, dst))

    def record_bugs(self, data, tc_id):
        # insert bug and found bug
        rc = data['returncode']
        reltime = data['reltime']
        new_bugs = set()
        # if returncode > 2 first bug is a triggered bug (crashing input)
        if abs(rc) > 2:
            crash = data['bugs'].pop(0)
            crash_bug_id = crash['bug_id']
            crash_src_line = crash['src_line']
            if crash_bug_id not in self.triggered_bugs:
                logger.info("Found bug id: %d, src: %s, returncode: %d", crash_bug_id, crash_src_line, rc)
                self.triggered_bugs.add(crash_bug_id)
            new_bugs.add((crash_bug_id, crash_src_line, rc))
        # remainder of logged bugs are reached bugs
        for bug in data['bugs']:
            if bug['bug_id'] not in self.reached_bugs:
                new_bugs.add((bug['bug_id'], bug['src_line'], 0))
                self.reached_bugs.add(bug['bug_id'])
        with db_proxy.atomic():
            for (bug_id, src_line, bug_rc) in new_bugs:
                (lb, _) = LavaBug.get_or_create(bug_id=bug_id, src_line=src_line, target=current_target)
                FoundBug.insert(reltime=reltime, returncode=bug_rc, bug=lb,
                                testcase=tc_id, experiment=current_experiment).execute()

    def write_stats(self, reltime=None):
        rt = reltime if reltime else round(datetime.now().timestamp() - self.start_time)
        logger.info("reltime: %d blocks:%d, edges:%d bugs:%d ",
                    rt, self.num_blocks(), self.num_edges(), self.num_bugs())
        if self.directed_mode:
            logger.info("  %d blocks left to discover", len(self.targets))
        results = {'mon_blocks': self.num_blocks(),
                   'mon_edges': self.num_edges(),
                   'mon_bugs': self.num_bugs(),
                   'mon_crashes': self.num_crashes}
        record_plotdata(rt, results)
        stats_file = os.path.join(fuzz_root, 'monitor_stats.json')
        with open(stats_file, 'w') as f:
            json.dump(results, f)

    def num_blocks(self):
        return len(self.observed_blocks)

    def num_edges(self):
        return len(self.observed_edges)

    def num_bugs(self):
        return len(self.triggered_bugs)

    def setup_target_list(self):
        self.targets.update([int(t) for t in target_blocks])

    def check_target_list(self, new_blocks, reltime):
        to_remove = set()
        for addr, size in new_blocks:
            for target in self.targets:
                if target >= addr and target <= addr + size:
                    to_remove.add(target)
                    logger.info("Found target 0x%x in basic block 0x%x (%d) at %ds.", target, addr, size, reltime)
        self.targets.difference_update(to_remove)

        if len(self.targets) == 0:
            logger.info("Found all target blocks!  Shutting down. (reltime=%d)", reltime)
            os.system("/start_fuzzing --stop {}".format(config['session']))
            stop_soon.set()


def add_san_options(san_env, values):
    for options in ['ASAN', 'UBSAN', 'MSAN', 'LSAN']:
        san_env['{}_OPTIONS'.format(options)] = "abort_on_error={}:symbolize={}".format(*values)
    return san_env


def run_target(cmd, use_stdin=None, timeout=30, check_lava=False):
    # Run command
    args = shlex.split(cmd)

    asan_env = os.environ.copy()
    asan_env = add_san_options(asan_env, (1, 0))

    proc = Popen(args, stdin=use_stdin, stdout=PIPE, stderr=PIPE, env=asan_env)
    resp_out, resp_err = (b"", b"")
    try:
        resp_out, resp_err = proc.communicate(timeout=timeout)
    except SubprocessError:
        logger.error("Timer expired, Killing: %s", cmd)
        proc.kill()
        # wait for stdout, stderr
        proc.wait()

    d = {'pid': proc.pid}
    if check_lava:
        d['returncode'] = proc.returncode
        if b'LAVA' in bug_marker:
            d['bugs'] = parse_lava_log(resp_err)
        if b'MAGMA' in bug_marker:
            d['bugs'] = parse_magma_log(resp_err)
    return d


def record_testcase(reltime, filename, blocks, edges, new_blocks, new_edges, returncode, size):
    args = {'reltime': reltime,
            'filename': filename,
            'blocks': blocks,
            'edges': edges,
            'new_blocks': new_blocks,
            'new_edges': new_edges,
            'returncode': returncode,
            'size': size,
            'experiment': current_experiment}
    with db_proxy.atomic():
        try:
            t_id = TestCase.insert(**args).execute()
        except pw.OperationalError as e:
            logger.error(e)
            t_id = None
    return t_id


def record_plotdata(reltime, data):
    args = {'reltime': reltime,
            'blocks': data.get('mon_blocks'),
            'edges': data.get('mon_edges'),
            'bugs': data.get('mon_bugs'),
            'crashes': data.get('mon_crashes'),
            'experiment': current_experiment}
    with db_proxy.atomic():
        try:
            PlotData.insert(**args).execute()
        except pw.OperationalError as e:
            logger.error(e)


def record_bbs(block_set, reltime, tc_id):
    # Bulk insert into DB
    # logger.debug("Inserting %d blocks into DB for %s as part of %s",
    #              len(block_set), current_target, current_experiment)
    flist = list()
    with db_proxy.atomic():
        for (block_start, size) in block_set:
            (b, _) = Block.get_or_create(address=block_start, target=current_target, defaults={'size': size})
            flist.append(FoundBlock(reltime=reltime, testcase=tc_id, block=b, experiment=current_experiment))
        FoundBlock.bulk_create(flist)


def record_edges(edge_set, reltime, tc_id):
    # Bulk insert into DB
    # logger.debug("Inserting %d edges into DB", len(edge_set))
    flist = list()
    with db_proxy.atomic():
        for (old_bb, new_bb) in edge_set:
            (e, _) = Edge.get_or_create(start=old_bb, end=new_bb, target=current_target)
            flist.append(FoundEdge(reltime=reltime, testcase=tc_id, edge=e, experiment=current_experiment))
        FoundEdge.bulk_create(flist)


def shahexdigest(filepath, hash_name='sha1'):
    BLOCKSIZE = 65536
    m = hashlib.new(hash_name)
    with open(filepath, 'rb') as f:
        data = f.read(BLOCKSIZE)
        while len(data) > 0:
            m.update(data)
            data = f.read(BLOCKSIZE)
    return m.hexdigest()


def bitness(filepath):
    with open(filepath, "rb") as f:
        data = f.read(32)
    ei_class = data[4]  # 1=32, 2=64
    if ei_class in [1, 2]:
        return ei_class * 32
    e_machine = data[18]
    raise Exception("[!] Unexpected value in elf header: %s (ei_class: %d, e_machine: 0x%x)" %
                    (filepath, e_machine, ei_class))


def parse_lava_log(log):
    try:
        lava = log.rfind(bug_marker)
    except AttributeError:
        lava = -1
    bugs = list()
    while lava >= 0:
        r = dict()
        msg = log[lava:].split(maxsplit=3)
#       r['bug_id'] = int(msg[1].strip()[:-1].decode('utf-8'))
        r['bug_id'] = int(digits_re.match(msg[1].decode('utf-8')).group(1))
        r['src_line'] = msg[2].split(b'\n')[0].strip().decode('utf-8')
        bugs.append(r)
        lava = log.rfind(bug_marker, 0, lava)
    return bugs


def parse_magma_log(log):
    try:
        lava = log.rfind(bug_marker)
    except AttributeError:
        lava = -1
    bugs = list()
    while lava >= 0:
        r = dict()
        msg = log[lava:].split(maxsplit=3)
#       r['bug_id'] = int(msg[1].strip()[:-1].decode('utf-8'))
        magma_bug_id = msg[1].decode('utf-8')[:-1]
        r['bug_id'] = int(digits_re.match(magma_bug_id).group(1))
        r['src_line'] = magma_bug_id
        bugs.append(r)
        lava = log.rfind(bug_marker, 0, lava)
    return bugs


def get_database_kwargs(db_config):
    db_pw = os.getenv('DB_PASSWD')
    if db_pw is None:
        db_pw = db_config.get('db_passwd')
        if db_pw is None:
            logger.warning("No DB_PASSWD set in enviornment or config, trying with no password")

    # Only specify a password argument if one exists, otherwise postgres tries using a blank password
    # instead of no password
    db_kwargs = {"user": db_config['user'],
                 "host":  db_config['host']}
    if db_pw:
        db_kwargs['password'] = db_pw

    # Optionally specify the connection port
    if 'port' in db_config:
        db_kwargs['port'] = db_config['port']

    return db_kwargs


def initialize_database():
    assert('database' in config), "database section not found in {}".format(sys.argv[2])
    db_config = config['database']

    if db_config.get('type') == 'sqlite':
        db_file = os.path.join(fuzz_root, 'monitor_cov.db')
        db = pw.SqliteDatabase(db_file, pragmas={'journal_mode': 'wal', 'cache_size': -1024 * 64})
    else:
        db_kwargs = get_database_kwargs(db_config)
        db = pw.PostgresqlDatabase(db_config['name'], **db_kwargs)

    # Configure proxy to use the configured database
    db_proxy.initialize(db)

    db_proxy.create_tables([
        Target, Experiment, TestCase, PlotData,
        Block, Edge, FoundBlock, FoundEdge, LavaBug, FoundBug])

    target_name = config['session']
    experiment_name = sys.argv[1]
    # get hash of target binary
    shahash = shahexdigest(tgt_binary)
    # Load or create target, create experiment
    global current_target, current_experiment, config_arch
    # get bitness (arch) of target
    config_arch = bitness(tgt_binary)

    (current_target, _) = Target.get_or_create(name=target_name, arch=config_arch, shahash=shahash)
    (current_experiment, is_new) = Experiment.get_or_create(name=experiment_name, target=current_target)
    if not is_new:
        raise RuntimeError("An experiment named {} already exists".format(current_experiment))

    logger.info("Watching %s for new inputs for experiment %s with target %s",
                fuzz_root, experiment_name, target_name)


def write_pid_file():
    with open('.monitor_pid', 'w') as f:
        f.write("{}".format(os.getpid()))


def handle_sigint(signum, frame):
    logger.info("Received signal %d", signum)
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    os.kill(os.getppid(), signal.SIGUSR1)
    stop_soon.set()


def main(experiment_name):
    initialize_database()

    signal.signal(signal.SIGINT, signal.SIG_IGN)
    write_pid_file()

    cmd_args = config.get('cmdline', '')
    if 'cov_cmdline' in config:
        cmd_args = config['cov_cmdline']

    commands = ["{} {}".format(tgt_binary, cmd_args),
                "{} {}".format(asan_binary, cmd_args)]

    # queue for inputs to be processed
    file_queue = multiprocessing.Queue()

    # Setup a thread to update coverage as new inputs are created
    observer = Watcher(file_queue, fuzz_root)

    # Start a process to execute input files
    executor = Forkserver(file_queue, commands)

    executor.start()
    observer.start()

    # Wait forever, processing new inputs until we get a Ctrl-C
    signal.signal(signal.SIGINT, handle_sigint)

    observer.join()
    executor.join()

    try:
        os.rmdir(trace_logdir)
    except OSError:
        logger.error("drcov log directory not empty.")
        os.system("ls {}".format(trace_logdir))
        shutil.rmtree(trace_logdir, ignore_errors=True)


if __name__ == '__main__':
    main(sys.argv[1])
