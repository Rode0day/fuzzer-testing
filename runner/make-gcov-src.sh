#!/bin/bash

[ -d src ] || exit 1

NOW=$(date +"%Y%m%d_%H%M")
LOGF="/tmp/build_${USER}_${NOW}.log"

configure_make_src() {
    make -C src clean >/dev/null
    rm -rf $1
    cp -r src $1
    if [[ -e ${1}/configure ]]; then
        pushd "$1" >/dev/null
        ./configure $2 >/dev/null
        popd >/dev/null
    fi
    make -C $1 >${LOGF} 2>&1
}

make_gcov(){
    export CC=gcc
    export CFLAGS="-m32 -fprofile-arcs -ftest-coverage -fPIC -O0 -w"
    configure_make_src gcov --disable-shared
}

make_ccov() {
    [ ! $(which clang) ] && echo "[-] clang not found!" && exit 1
    export CC=clang
    export CFLAGS="-m32 -fprofile-instr-generate -fcoverage-mapping -fPIC -O0 -w"
    configure_make_src ccov --disable-shared
}

make_scov() {
    [ ! $(which clang) ] && echo "[-] clang not found!" && exit 1
    export CC=clang 
    export CFLAGS="-m32 -fsanitize=address -fsanitize-coverage=trace-pc-guard -fPIC -O0 -w" 
    configure_make_src scov --disable-shared
}

make_asan() {
    [ ! $(which clang) ] && echo "[-] clang not found!" && exit 1
    export CC=clang 
    export CFLAGS="-m32 -fsanitize=address -fno-omit-frame-pointer -g -O1 -w" 
    configure_make_src asan --disable-shared
}


if [ $# -eq 0 ]; then
    make_gcov
elif [ "$1" = "ccov" ]; then
    make_ccov
elif [ "$1" = "scov" ]; then
    make_scov
elif [ "$1" = "asan" ]; then
    make_asan
else
   echo "Usage: $0 [ ccov | scov | asan ]"
   exit 1
fi

if [ $? -eq 0 ]
then
    rm -f ${LOGF}
else
    # if the build failed, dump the tail of the log file
    echo "[!] Building coverage binary failed."
    [ -e $LOGF ] && tail -n 20 $LOGF
    exit 1
fi


