#!/usr/bin/env python3

import argparse
import json
import os
from pathlib import Path
import shlex
from subprocess import Popen, PIPE
import sys
import time


def check_elf64(binary_path):
    if os.path.exists(binary_path):
        with open(binary_path, 'rb') as f:
            d = f.read(5)
            if len(d) > 4:
                return d[4]
    raise Exception("[!] failed to identify elf binary")


def read_config(config_file):
    if not os.path.isfile(config_file):
        print("[-] Error: config file not found!")
        sys.exit(1)

    with open(config_file, 'r') as f:
        config = json.load(f)

    config['start_time'] = os.getenv('START_TIME', time.time())
    config['monitor_pid'] = os.getenv('MON_PID', 0)

    return config


def export_vars(config):
    exported = {
        'FUZZER_BIN': 'fuzzer',
        'INPUTS': 'input',
        'OUTPUTS': 'output',
        'BINARY': 'target',
        'CMDLINE': 'cmdline',
        'DICT': 'dict',
        'TGT_NAME': 'name',
        'EXEC_TIMEOUT': 'timeout',
        'MORE_ARGS': 'afl_margs',
        'START_TIME': 'start_time',
        'MON_PID': 'monitor_pid'
        }
    for k, v in exported.items():
        if v in config:
            print('export {}="{}"'.format(k, config[v]))
    if config.get('qemu'):
        print('export {}="{}"'.format('QEMU', '1'))


def parse_args():
    p = argparse.ArgumentParser(description="simple fuzzer launcher; default action is to export variables")
    p.add_argument("-s", "--start", default=None, type=str, help="start supported fuzzer [angora,eclipser,honggfuzz]")
    p.add_argument("-M", "--monitor", default=None, type=str, help="unique job name; start coverage monitor")
    p.add_argument("-c", "--capture", help="capture pane from tmux")
    p.add_argument("-N", dest="n", default="2", type=str, help="number of fuzzing instances (cpu cores)")
    p.add_argument("--max-file-len", default="1048576",
                   type=str, help="maximum input size (default=1MB)")
    p.add_argument("-V", "--time-limit", dest="max_run_time", default="86400",
                   type=str, help="total fuzzing duration (secs)")
    p.add_argument("config_file", type=str, help="job config file (json)")
    return p.parse_args()


class fuzzer_control:
    def __init__(self, args, config):
        self.fuzzer = config['fuzzer']
        self.name = config['session']
        self.binary = config['target']
        self.seed_dir = config['input']
        self.out_dir = config['output']
        self.work_dir = os.path.dirname(os.path.abspath(args.config_file))
        self.max_run_time = str(config['time_limit']) if 'time_limit' in config else args.max_run_time
        self.max_file_len = args.max_file_len
        self.timeout = str(config['timeout']) if 'timeout' in config else None
        self.n = args.n
        self.qemu = config.get('qemu')
        self.elf64 = check_elf64(os.path.join(self.work_dir, self.binary)) == 2
        self.use_stdin = '@@' not in config['cmdline']
        self.cmdline = config['cmdline']
        self.dict = config.get('dict')
        self.more_args = config.get('afl_margs')

    def setup_tmux(self):
        cmd = ["tmux", "new-session", "-d",
               "-s", self.name,
               "-c", self.work_dir,
               "-x", "180", "-y", "80", "/bin/bash"]
        p = Popen(cmd)
        out, err = p.communicate()
        setw = ['tmux', 'set', '-g', 'remain-on-exit', 'on']
        Popen(setw)

    def capture_pane(self):
        p = Popen(['tmux', 'list-pane', '-sF', "'#D #W'", '-t', self.name],
                  stdout=PIPE, stderr=PIPE, universal_newlines=True)
        out, err = p.communicate()

        cmd = "tmux capture-pane -ept {} > {}/console.log"
        pane_id = "%2"
        for word in out.replace("'", "").split():
            if self.name in word:
                p = Popen(cmd.format(pane_id, self.out_dir), shell=True)
                p.wait()
                break
            pane_id = word

    def start_monitor(self, job_name, config_file):
        self.setup_tmux()
        monitor_script = '/usr/local/bin/monitor.py'
        if not os.path.isfile(monitor_script):
            return
        window_name = "cov_{}".format(self.name)
        cmd = ["tmux", "new-window", "-ad",
               "-t", self.name,
               "-n", window_name,
               "-c", self.work_dir,
               monitor_script, job_name, config_file]
        Popen(cmd)

    def add_to_job_vars(self, d):
        with open('.jobvars', 'a') as f:
            for k, v in d.items():
                f.write("export {}={}\n".format(k, v))

    def set_sync_dir(self, instance_name, make_dirs=False, start_tmux=True):
        if start_tmux:
            self.setup_tmux()
        self.sync_dir = os.path.join(self.out_dir, instance_name)
        self.queue_dir = os.path.join(self.sync_dir, 'queue')
        self.crashes_dir = os.path.join(self.sync_dir, 'crashes')
        self.cmd = ["tmux", "new-window", "-adP",
                    "-t", self.name,
                    "-n", instance_name,
                    "-F", "#{pane_pid}",
                    "-c", self.work_dir]
        if make_dirs:
            Path(self.crashes_dir).mkdir(parents=True, exist_ok=True)
            Path(self.queue_dir).mkdir(parents=True, exist_ok=True)
            f_stats_path = os.path.join(self.sync_dir, 'fuzzer_stats')
            Path(f_stats_path).touch(exist_ok=True)

    def start_angora(self):
        instance_name = "{}_angora".format(self.name)
        self.set_sync_dir(instance_name)

        self.cmd.extend([
            "/angora/angora_fuzzer",
            "--sync_afl",        # sync seeds with AFL
            "-i", self.seed_dir,
            "-o", self.out_dir,
            "-t", "{}.tt".format(self.binary),
            "-j", self.n])
        if 'afl' in self.fuzzer:
            self.cmd.append('-A')  # disable AFLs random mutation in Angora
        if self.timeout:
            self.cmd.extend(['--time_limit', str(int(self.timeout)/1000)])
        if self.more_args:
            self.cmd.extend(self.more_args)
        self.cmd.append("--")
        self.cmd.append(self.binary)
        self.cmd.extend(shlex.split(self.cmdline))
        p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
        out, errr = p.communicate()
        self.add_to_job_vars({'FUZZER_PID': out.strip()})

    def start_parmesan(self):
        instance_name = "{}_parmesan".format(self.name)
        self.set_sync_dir(instance_name)
        binary_dir = os.path.dirname(self.binary)

        self.cmd.extend([
            "/parmesan/bin/fuzzer",
            "--sync_afl",        # sync seeds with AFL
            "-i", self.seed_dir,
            "-o", self.out_dir,
            "-c", os.path.join(binary_dir, "targets.pruned.json"),
            "-t", "{}.track".format(self.binary),
            "-s", "{}.san.fast".format(self.binary),
            "-j", self.n])
        if 'afl' in self.fuzzer:
            self.cmd.append('-A')  # disable AFLs random mutation in Angora
        if self.timeout:
            self.cmd.extend(['--time_limit', str(int(self.timeout)/1000)])
        if self.more_args:
            self.cmd.extend(self.more_args)
        self.cmd.append("--")
        self.cmd.append("{}.fast".format(self.binary))
        self.cmd.extend(shlex.split(self.cmdline))
        p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
        out, errr = p.communicate()
        self.add_to_job_vars({'FUZZER_PID': out.strip()})

    def start_symcc(self):
        binary_dir = os.path.dirname(self.binary)
        for afl_dir in os.scandir(self.out_dir):
            if afl_dir.is_file() or self.name not in afl_dir.name:
                continue

            instance_num = int(afl_dir.name.replace(self.name, '0'))
            instance_name = "{}_Q{:02}".format(self.name, instance_num)
            self.set_sync_dir(instance_name, start_tmux=False)

            self.cmd.extend([
                "/usr/local/bin/symcc_fuzzing_helper",
                "-o", self.out_dir,    # AFL sync dir
                "-a", afl_dir.name,    # name of AFL fuzzer directory
                "-n", instance_name])
            self.cmd.append("--")
            self.cmd.append(self.binary)
            self.cmd.extend(shlex.split(self.cmdline))
            p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
            out, errr = p.communicate()
            self.add_to_job_vars({'FUZZER_PID': out.strip()})

    def start_ankou(self):
        instance_name = "{}_ankou".format(self.name)
        self.set_sync_dir(instance_name)
        Path(self.sync_dir).mkdir(parents=True, exist_ok=True)
        ankou_out_dir = os.path.join(self.out_dir, 'ankou')
        self.cmd.extend([
            "Ankou",
            "-app", self.binary,
            "-i", self.seed_dir,
            "-o", ankou_out_dir,
            "-dur", "{}s".format(self.max_run_time),
            "-threads", self.n])
        if not self.use_stdin:
            self.cmd.extend(["-args", self.cmdline])
        print("[*] Executing: {}".format(' '.join(self.cmd)))
        p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
        out, errr = p.communicate()
        self.add_to_job_vars({'FUZZER_PID': out.strip()})
        while not os.path.isdir(ankou_out_dir) or len(os.listdir(ankou_out_dir)) < 2:
            time.sleep(1)  # wait for fuzzer to start
        _, bname = os.path.split(self.binary)
        ankou_crashdir = "../ankou/crashes-{}".format(bname)
        os.symlink(ankou_crashdir, self.crashes_dir)
        ankou_seed_dir = "../ankou/seeds-{}".format(bname)
        os.symlink(ankou_seed_dir, self.queue_dir)

    def start_eclipser(self):
        instance_name = "{}_eclipser".format(self.name)
        self.set_sync_dir(instance_name, make_dirs=True)

        self.cmd.extend([
               "dotnet", "/opt/Eclipser/Eclipser.dll",
               "-p", self.binary,
               "-i", self.seed_dir,
               "-s", self.out_dir,
               "-o", self.sync_dir,
               "-t", self.max_run_time,
               "--nspawn", "10",
               "--nsolve", "600"])
        tgt_args = self.cmdline.replace('@@', '.input')
        if self.timeout:
            self.cmd.extend(['-e', self.timeout])
        if len(tgt_args) > 0:
            self.cmd.extend(["--arg", tgt_args])
        if not self.use_stdin:
            self.cmd.extend(["-f", ".input"])
        if not self.elf64:
            self.cmd.extend(["--architecture", "x86"])
        print("[*] Executing: {}".format(' '.join(self.cmd)))
        # p0 = Popen(['/usr/local/bin/decode_files.py', self.sync_dir])
        p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
        out, errr = p.communicate()
        # self.add_to_job_vars({'FUZZER_PID': out.strip(), 'DECODE_PID': p0.pid})
        self.add_to_job_vars({'FUZZER_PID': out.strip()})

    def start_honggfuzz(self):
        instance_name = "{}_honggfuzz".format(self.name)
        self.set_sync_dir(instance_name, make_dirs=True)

        self.cmd.extend([
               "honggfuzz",
               "-i", self.seed_dir,
               "--output", self.queue_dir,
               "-n", self.n,
               "-W", self.crashes_dir,
               "--max_file_size", self.max_file_len])
        if self.dict:
            self.cmd.append("-w")
            self.cmd.append(self.dict)
        if self.timeout:
            self.cmd.extend(["--timeout", str(int(self.timeout)/1000)])
        if self.use_stdin:
            self.cmd.append("--stdin_input")
        if self.more_args:
            self.cmd.extend(self.more_args)
        self.cmd.append("--")
        if self.qemu:
            qemu_bin = '/usr/local/bin/qemu-x86_64' if self.elf64 else '/usr/local/bin/qemu-i386'
            self.cmd.append(qemu_bin)
        self.cmd.append(self.binary)
        self.cmd.extend(shlex.split(self.cmdline.replace('@@', '___FILE___')))

        print("[*] Executing: {}".format(' '.join(self.cmd)))
        p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
        out, errr = p.communicate()
        self.add_to_job_vars({'FUZZER_PID': out.strip()})

    def start_nautilus(self):
        instance_name = "{}_nautilus".format(self.name)
        self.set_sync_dir(instance_name, make_dirs=True)

        self.cmd.extend([
               "fuzzer",
               "-o", self.out_dir])
        if self.more_args:
            self.cmd.extend(self.more_args)
        self.cmd.append("--")
        self.cmd.append(self.binary)
        self.cmd.extend(shlex.split(self.cmdline))
        print("[*] Executing: {}".format(' '.join(self.cmd)))
        p = Popen(self.cmd, stdout=PIPE, universal_newlines=True)
        out, errr = p.communicate()
        self.add_to_job_vars({'FUZZER_PID': out.strip()})


def main():
    args = parse_args()
    config = read_config(args.config_file)
    start_func_str = "start_{}".format(args.start)
    if args.start:
        fc = fuzzer_control(args, config)
        getattr(fc, start_func_str)()
    elif args.capture:
        fc = fuzzer_control(args, config)
        fc.capture_pane()
    elif args.monitor:
        fc = fuzzer_control(args, config)
        fc.start_monitor(args.monitor, args.config_file)
    else:
        export_vars(config)


if __name__ == '__main__':
    main()
