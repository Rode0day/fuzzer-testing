#!/bin/bash

FZ=angora
FZs=ang

# --sync_afl to allow sync seeds with AFL
# -A to disable AFL's random mutation in Angora.
# /angora/angora_fuzzer --sync_afl -A -i seeds -o output -t ./track/install/bin/file -- ./fast/install/bin/file -m ./fast/install/share/misc/magic.mgc @@

start_angora() {
    if [[ "$FUZZER_BIN" == *"afl"* ]]; then
        sed -i 's/master_instances":.*,/master_instances": 0,/' afl_job.json
        sed -i 's/dict/_dict/' afl_job.json
        local ANG_N="$(( $N - 1 ))"
        afl-multicore -c afl_job.json start 1
    else
        local ANG_N="$N"
    fi

    /usr/local/bin/job_control.py --start angora ${FZ}_job.json -N $ANG_N
    source .jobvars
    OUTPUTS="${OUTPUTS:-outputs}"
    SYNCDIR="$OUTPUTS/${FZ}"
    
    sleep 1s
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} > ${OUTPUTS}/initial_console.log
}

stop_angora() {
    [ -e .jobvars ] && source .jobvars
    TGT=${TGT:-$1}
    local outdir=${OUTPUTS:-outputs}
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} | tee ${outdir}/final_console.log
    tmux send-keys -t ${TGT}:${TGT}_${FZ} C-c
    if [[ "$FUZZER_BIN" == *"afl"* ]]; then
        afl-multikill -c afl_job.json
    fi
}

check_angora() {
    [ -e .jobvars ] && source .jobvars
    if [[ -n $FUZZER_PID ]]; then
        echo -ne "[*] ANGORA "
        ps -o pid,ppid,pgid,start,time,stat,args -p $FUZZER_PID
    fi
}

process_results() {
    local LOGDIR=${SYNCDIR:-outputs}
    /usr/local/bin/write_stats_file.py ${LOGDIR}/chart_stat.json > ${LOGDIR}/fuzzer_stats
    afl-stats -c ${FZ}_job.json -d stats.db -r
    tmux capture-pane -ept ${TGT}:${TGT}_${FZ} -E 28 > ${LOGDIR}/console.log
}

