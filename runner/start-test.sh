#!/bin/bash

# variable defaults
FZ=${FZ:-test}
iterations=0


run_test(){
    mkdir -p $Q_DIR
    mkdir -p $C_DIR
    sleep 10
    if [ -d orig ]; then
        for orig in orig/*; do
            cp ${orig} $Q_DIR/
            ((iterations++))
            sleep 5
        done
    fi

    sleep 30

    for seed in ${INPUTS}/*; do
        cp ${seed} $Q_DIR/
        ((iterations++))
        sleep  5
    done

    if [ -d tests ]; then
        sleep 30
        local n="$(ls tests | wc -l )"
        local delay="$(( 300 / $n )).1s"
        for poc in tests/*; do
            cp ${poc} $Q_DIR/
            ((iterations++))
            sleep 0.1s
        done

    fi

    if [ -d solutions ]; then
        sleep 30
        local n="$(ls solutions | wc -l )"
        local delay="$(( 300 / $n )).1s"
        for sol in solutions/*; do
            cp ${sol} $C_DIR/
            ((iterations++))
            sleep $delay
        done
    fi

    if [ -d real_bugs ]; then
        sleep 30
        local n="$(ls real_bugs | wc -l )"
        local delay="$(( 300 / $n )).1s"
        for poc in real_bugs/*; do
            cp ${poc} $C_DIR/
            ((iterations++))
            sleep $delay
        done

    fi

    exit 0
}

start_test() {
    SYNCDIR="$OUTPUTS/${TGT}_${FZ}"
    Q_DIR="$SYNCDIR/queue"
    C_DIR="$SYNCDIR/crashes"

    run_test &
    FUZZER_PID="$!" 

}

stop_test(){
    TGT=${TGT:-$1}
}

check_test(){
   afl-stats -c ${FZ}_job.json -r
}

process_results() {
    local LOGDIR=${SYNCDIR:-outputs}
    write_stats_file > $LOGDIR/fuzzer_stats
    afl-stats -c ${FZ}_job.json -d stats.db -r
}

write_stats_file() {
local last_path="$(cd $Q_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local last_crash="$(cd $C_DIR; ls -t | head -n1 | stat -c "%Y" - )"
local queue="$(cd $Q_DIR; ls | wc -l )"
local crashes="$(cd $C_DIR; ls | wc -l )"
local init="$(cd $INPUTS; ls | wc -l )"
local banner="${TGT}_${FZ}"
local cmdline="${BINARY} ${CMDLINE}"

echo "start_time        : $START_TIME
last_update       : $(date +"%s")
fuzzer_pid        : ${FUZZER_PID:-0}
cycles_done       : 0
execs_done        : ${iterations:-0}
execs_per_sec     : ${avg:-0}
paths_total       : ${queue:-0}
paths_favored     : 0
paths_found       : $(( $queue - $init ))
paths_imported    : 0
max_depth         : 0
cur_path          : 0
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : ${cov_pct:-0.0}
unique_crashes    : ${c_unique:-0}
unique_hangs      : ${timeouts:-0}
last_path         : $last_path
last_crash        : $last_crash
last_hang         : 0
execs_since_crash : 0
exec_timeout      : ${tmout:-0}
afl_banner        : ${banner}
afl_version       : v1.0
target_mode       : test
command_line      : $cmdline" 
}
