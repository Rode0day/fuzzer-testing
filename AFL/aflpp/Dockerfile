ARG BASE_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/base:18.04
ARG BUILDER_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/builder:18.04
FROM $BUILDER_IMAGE as builder

ARG DISTRO
ARG CLANG_CC
ARG CLANG_CXX
ARG LLVM_VER
ARG LLVM_LIB

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install dependencies
RUN wget -qO- 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xBA9EF27F' | apt-key add - && \
    echo "deb http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu ${DISTRO} main" | tee /etc/apt/sources.list.d/toolchain_ppa.list
RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      gcc-9-plugin-dev \
      g++-9 \
      clang-${LLVM_VER} \
      lld-${LLVM_VER} \
      llvm-${LLVM_VER}-dev \
      python3.8-dev \
      python3.8-venv

# Install AFL++
ARG CACHE_BUST=ce673cc
ARG GITREPO="-b 3.11c https://github.com/AFLplusplus/AFLplusplus.git"
RUN git clone --depth=1 ${GITREPO} /afl && \
    git -C /afl rev-parse HEAD | tee /usr/local/aflpp_commit_hash && \
    wget -qO /tmp/ninja-build.zip "https://github.com/ninja-build/ninja/releases/download/v1.10.2/ninja-linux.zip" && \
    python3 -m zipfile -e /tmp/ninja-build.zip /usr/bin/ && \
    chmod +x /usr/bin/ninja && \
    rm /tmp/ninja-build.zip
ENV LLVM_CONFIG=${LLVM_LIB}/bin/llvm-config \
    CC=gcc-9 \
    CXX=g++-9 \
    AFL_NO_X86=1
RUN echo "[**] BUILDING AFL"; \
    echo "CC=$(which ${CLANG_CC}) CXX=$(which ${CLANG_CXX}) make -C /afl all llvm" && \
    python3.8 -m pip install -U pip setuptools wheel && \
    ln -fs $(which python3.8) /usr/bin/python3 && \
    CC=$(which ${CLANG_CC}) CXX=$(which ${CLANG_CXX}) make -C /afl all llvm && \
#   make -C /afl gcc_plugin && \
    make -C /afl all && \
    cd /afl/qemu_mode && \
    sed -i 's/target-list="/target-list="i386-linux-user,/' build_qemu_support.sh && \
    ./build_qemu_support.sh && \ 
    make -C /afl/utils/aflpp_driver && \
    cp /afl/utils/aflpp_driver/libAFLDriver.a /usr/local/lib/ && \
    make -C /afl install && \
    mkdir /usr/local/i386 && \
    rm -f /usr/local/bin/afl-qemu-trace && \
    ln /usr/local/bin/afl-* /usr/local/i386/ && \
    cp -f /afl/qemu_mode/qemuafl/build/x86_64-linux-user/qemu-x86_64 /usr/local/bin/afl-qemu-trace && \
    cp -f /afl/qemu_mode/qemuafl/build/i386-linux-user/qemu-i386 /usr/local/i386/afl-qemu-trace


### STAGE 2 ###
FROM $BASE_IMAGE

ARG LLVM_VER
ARG LLVM_LIB

RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      clang-${LLVM_VER} \
      lld-${LLVM_VER} \
      llvm-${LLVM_VER} \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    ln -fLs ${LLVM_LIB}/bin/clang /usr/bin/clang && \
    ln -fLs ${LLVM_LIB}/bin/clang++ /usr/bin/clang++ && \
    ln -fLs ${LLVM_LIB}/bin/llvm-config /usr/bin/llvm-config

COPY --from=builder /usr/local /usr/local

ENV LLVM_CONFIG=${LLVM_LIB}/bin/llvm-config \
    CC=afl-cc \
    CXX=afl-cc++ \
    AR=${LLVM_LIB}/bin/llvm-ar \
    RANLIB=${LLVM_LIB}/bin/llvm-ranlib \
    CFLAGS="" \
    CXXFLAGS="" \
    AFL_SKIP_CPUFREQ=1 \
    QEMU_RESERVED_VA=0xf700000 \
    FUZZER=afl \
    FZ=aflpp
WORKDIR /data
USER fuzz
ENTRYPOINT ["afl-fuzz"]
