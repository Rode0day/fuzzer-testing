# fuzzer-testing

Standardized Docker images for evaluating open-source fuzzers, fully integrated with 
the [corpora](https://gitlab.com/Rode0day/corpora) for easy experimentation.


## Supported / Included Fuzzers
- AFL [github.com/google/AFL](https://github.com/google/AFL)
- AFLplusplus [github.com/AFLplusplus](https://github.com/AFLplusplus/AFLplusplus)
- Honggfuzz [github.com/google/honggfuzzz](https://github.com/google/honggfuzz)
- QSYM [sslab-gatech/qsym](https://github.com/sslab-gatech/qsym)
- Eclipser [SoftSec-KAIST/Eclipser](https://github.com/SoftSec-KAIST/Eclipser)
- Ankou [SoftSec-KAIST/Ankou](https://github.com/SoftSec-KAIST/Ankou)
- Angora [AngoraFuzzer/Angora](https://github.com/AngoraFuzzer/Angora)
- Parmesan [vusec/parmesan](https://github.com/vusec/parmesan)
- SymCC [eurecom-s3/symcc](https://github.com/eurecom-s3/symcc)
- Weizz [andreafioraldi/weizz-fuzzer](https://github.com/andreafioraldi/weizz-fuzzer)

Docker images are available in the repository 
[container registry](https://gitlab.com/Rode0day/fuzzer-testing/container_registry).  



## Known Issues

### QSYM 
- Works best with an "old" kernel (~3.13 series) due to PIN version
- When running in docker the old kernel is required, but there is a
  conflict with an updated docker-ce and older kernels.
  See the known issues [here](https://docs.docker.com/engine/release-notes/#18096)

### Current Source commits:

```
afl                  :  61037103ae3722c8060ff7082994836a794f978e
aflpp                :  a7340a1ac6c6165c8eb390a503758104c0d85bcb
angora               :  3cedcac8e65595cd2cdd950b60f654c93cf8cc2e
eclipser             :  b6626bc9b087b6a62c076f5a8da68e69b267f374
honggfuzz            :  7eecfc991d0ae540d9773a6feb8fac5012a55ed6
qsym                 :  40ccd39e285eca3a0bf05a83cf6f2fd020d453bd
eclipser             :  b6626bc9b087b6a62c076f5a8da68e69b267f374
```
