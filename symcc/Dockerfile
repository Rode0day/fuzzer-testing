ARG BASE_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/base:18.04
ARG BUILDER_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/builder:18.04
ARG AFLPP_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/aflpp_runner:18.04
ARG AFL_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/afl_runner:18.04
ARG SYMCC_I386_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/i386/symcc:18.04
FROM $AFLPP_IMAGE as aflpp_image
FROM $AFL_IMAGE as afl_image
FROM $SYMCC_I386_IMAGE as symcc_i386_image
FROM $BUILDER_IMAGE as builder

ARG LLVM_BASE
ARG CLANG_CC
ARG CLANG_CXX

RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      cargo \
      lib32tinfo-dev \
      ${LLVM_BASE}-dev \
      ninja-build \
      python3-pip

RUN git clone --no-checkout https://github.com/eurecom-s3/symcc.git /symcc && \
    pip3 install lit && \
    cd /symcc && \
    git checkout master && \
    git -C /symcc rev-parse HEAD | tee /usr/local/symcc_commit_hash && \
    git submodule update --init && \
    mkdir /symcc/build /symcc/build32

RUN git clone --depth=1 -b 'z3-4.8.6' https://github.com/Z3Prover/z3.git /z3 && \
    mkdir -p /z3/build64 /z3/build32

RUN cd /z3/build64 && \
    cmake -G Ninja -DCMAKE_BUILD_TYPE=Release /z3 && \
    ninja && \
    ninja install

RUN cd /z3/build32 && \
    CFLAGS="-m32" CXXFLAGS="-m32" cmake -G Ninja -DCMAKE_BUILD_TYPE=Release /z3 && \
    ninja && \
    cp /z3/build32/libz3.so.4.8.6.0 /usr/lib32/

WORKDIR /symcc/build
### RUN sed -i '/uint64_t __uint64/d' /symcc/runtime/qsym_backend/pin.H
COPY --from=symcc_i386_image /usr/lib/${LLVM_BASE} /usr/lib32/${LLVM_BASE}
COPY --from=symcc_i386_image /usr/lib/i386-linux-gnu/libLLVM-*.so.1 /usr/lib32/${LLVM_BASE}/lib/
RUN cmake -G Ninja -DTARGET_32BIT=ON -DQSYM_BACKEND=ON \
          -DCMAKE_BUILD_TYPE=RelWithDebInfo \
          -DDZ3_DIR=/z3/build64 \
          -DLLVM_32BIT_DIR=/usr/lib32/${LLVM_BASE}/lib/cmake/llvm \
          -DZ3_32BIT_DIR=/z3/build32 \
          /symcc
RUN ninja
# RUN ninja check && echo "ALL GOOD!"

RUN cargo install --path /symcc/util/symcc_fuzzing_helper

COPY --from=afl_image /usr/local /usr/local

RUN ln /symcc/build/SymRuntime-prefix/src/SymRuntime-build/libSymRuntime.so \
       /usr/local/lib/ && \
    ln /symcc/build/SymRuntime32-prefix/src/SymRuntime32-build/libSymRuntime.so \
       /usr/lib32/ && \
    ln /symcc/build/libSymbolize.so /usr/local/lib/ && \
    ln /root/.cargo/bin/symcc_fuzzing_helper /usr/local/bin

RUN git clone --depth=1 https://github.com/eurecom-s3/symqemu.git /symqemu && \
    cd /symqemu && \
    /symqemu/configure \
      --audio-drv-list=                 \
      --disable-bluez                   \
      --disable-sdl                     \
      --disable-gtk                     \
      --disable-vte                     \
      --disable-opengl                  \
      --disable-virglrenderer           \
      --target-list=x86_64-linux-user   \
      --enable-capstone=git             \
      --symcc-source=/symcc             \
      --symcc-build=/symcc/build && \
    make &&  \
    cp x86_64-linux-user/symqemu-x86_64 /usr/local/bin/

### STAGE 2 ###
FROM $BASE_IMAGE

ARG LLVM_BASE
ARG CLANG_CC

RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      libstdc++6 \
      lib32stdc++6 \
      lib32gomp1 \
      lib32tinfo5 \
      libtinfo5 \
      ${CLANG_CC} \
      ${LLVM_BASE} \
      zlib1g \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

COPY --from=builder /usr/local /usr/local
COPY --from=builder \
         /usr/lib32/libz3.so.4.8.6.0 \
         /usr/lib32/libSymRuntime.so \
         /usr/lib32/
COPY symcc sym++ /usr/local/bin/


ENV AFL_SKIP_CPUFREQ=1 \
    AFL_CC=${CLANG_CC} \
    AFL_CXX=${CLANG_CXX} \
    CC=symcc \
    CXX=sym++ \
    QEMU_RESERVED_VA=0xf700000 \
    FUZZER=symcc
WORKDIR /data
USER fuzz
ENTRYPOINT [ "/bin/bash" ]
