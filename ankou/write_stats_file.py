#!/usr/bin/env python3

import os
import re
import sys
import time

stats_file = """start_time        : {start_time}
last_update       : {last_update}
fuzzer_pid        : {fuzzer_pid}
cycles_done       : {cycles_done}
execs_done        : {execs_done}
execs_per_sec     : {execs_per_sec}
paths_total       : {paths_total}
paths_favored     : 0
paths_found       : {paths_found}
paths_imported    : 0
max_depth         : 0
cur_path          : 0
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : {cov_pct}
unique_crashes    : {unique_crashes}
unique_hangs      : {unique_hangs}
last_path         : {last_path}
last_crash        : {last_crash}
last_hang         : 0
execs_since_crash : 0
exec_timeout      : {exec_timeout}
afl_banner        : {banner}
afl_version       : v1.2.2
target_mode       : ankou
command_line      : {cmdline}"""

if (len(sys.argv) != 2 or not os.path.isfile(sys.argv[1])):
    print("Usage: {} <path_to_ankou_status_dir/seed_manager.csv>".format(sys.argv[0]))
    sys.exit(1)


def last_file(path):
    size = len(os.listdir(path))
    most_recent = max(os.scandir(path), key=lambda x: x.stat().st_mtime, default=None)
    if most_recent:
        return int(most_recent.stat().st_mtime), size
    return 0, size


def last_line(path):
    with open(path) as f:
        for line in f:
            pass
        return line


def parse_console():
    cf = sys.argv[2] 
    if not os.path.isfile(cf):
        return {}
    with open(cf, 'r') as fd:
        data = fd.read()

    rx = re.compile(r'Target: (?P<tgt_bin>.*)\n'
                    r'round number: (?P<rounds>[0-9]*) \((?P<runtime>.*)\)\n'
                    r'len\(seedPts\) = (?P<len_seeds>.*) - throughput: (?P<throughput>.*)\n'
                    r'total unique crashes: (?P<crashes>.*) - hangs: (?P<hangs>.*)\n'
                    r'#edges: (?P<n_edges>.*) \((?P<cov_pct>.*)\) - rate: (?P<rate>.*)\n')
    match = rx.search(data)
    return match.groupdict() if match else {}


env = os.environ
queue_dir = env.get('Q_DIR', '.')
crash_dir = env.get('C_DIR', '.')
sync_dir = env.get('SYNCDIR', '.')
last_path, q_size = last_file(queue_dir)
last_crash, num_crashes = last_file(crash_dir)
num_inputs = len(os.listdir(env.get('INPUTS', 'inputs')))

mem, seed_n, file_size_mean, tr_size_mean, exec_time_mean, execN, etime = last_line(sys.argv[1]).split(',')

m = parse_console()

eps = round(float(execN) / float(etime), 2)

now = int(time.time())
stats = dict()

stats['start_time'] = env.get('START_TIME', now)
stats['fuzzer_pid'] = env.get('FUZZER_PID', 0)
stats['last_update'] = now
stats['cycles_done'] = m.get('round', 0)
stats['execs_done'] = execN
stats['execs_per_sec'] = m.get('throughput', eps)
stats['paths_total'] = q_size
stats['paths_found'] = q_size - num_inputs
stats['unique_crashes'] = num_crashes
stats['unique_hangs'] = m.get('hangs', 0)
stats['last_path'] = last_path
stats['last_crash'] = last_crash
stats['cov_pct'] = m.get('cov_pct', '0.0%')
stats['exec_timeout'] = env.get('EXEC_TIMEOUT', 1)
stats['banner'] = "{}_ankou".format(env.get('TGT', ""))
stats['cmdline'] = "{} {}".format(env.get('BINARY', ""), env.get('CMDLINE', ""))

print(stats_file.format(**stats))
