#!/usr/bin/env python3

import base64
import json
import os
import sys
from pathlib import Path
from inotify_simple import INotify, flags


def decode_file(fp_in, fp_out):
    with open(fp_in, 'r') as fd:
        tc = json.load(fd)
    if len(tc.get('filecontent')) > 0:
        data = tc['filecontent']
    else:
        data = tc['stdin']
    with open(fp_out, 'wb') as fd:
        fd.write(base64.b64decode(data))


def watch(fuzzer_dir):
    root_path, tgt = os.path.split(fuzzer_dir)
    tc_path = os.path.join(root_path, 'testcase')
    cr_path = os.path.join(root_path, 'crash')
    q_dir = os.path.join(fuzzer_dir, 'queue')
    c_dir = os.path.join(fuzzer_dir, 'crashes')
    inotify = INotify()
    created = set()
    f_create_close = flags.CREATE | flags.CLOSE_WRITE

    Path(tc_path).mkdir(parents=True, exist_ok=True)
    Path(cr_path).mkdir(parents=True, exist_ok=True)
    Path(q_dir).mkdir(parents=True, exist_ok=True)
    Path(c_dir).mkdir(parents=True, exist_ok=True)

    print("Watching {} for new testcases".format(tc_path))
    q_wd = inotify.add_watch(tc_path, f_create_close)
    c_wd = inotify.add_watch(cr_path, f_create_close)

    while True:
        for e in inotify.read():
            if e[1] & flags.ISDIR:
                continue
            wd_name = (e[0], e[3])
            if e[1] & flags.CREATE:
                created.add(wd_name)
                continue
            if e[1] & flags.CLOSE_WRITE and wd_name in created:
                created.remove(wd_name)
            else:
                continue

            if e[0] == q_wd:
                in_path = os.path.join(tc_path, e[3])
                out_path = os.path.join(q_dir, e[3])
                decode_file(in_path, out_path)
            if e[0] == c_wd:
                in_path = os.path.join(cr_path, e[3])
                out_path = os.path.join(c_dir, e[3])
                decode_file(in_path, out_path)


if __name__ == '__main__':
    if (len(sys.argv) != 2 or not os.path.isdir(sys.argv[1])):
        print("Usage: {} <fuzzer_directory>".format(sys.argv[0]))
        sys.exit(1)
    watch(sys.argv[1])
