ARG BASE_IMAGE
FROM $BASE_IMAGE
LABEL maintainer="jmb@iseclab.org" \
      type="base"

ARG DISTRO
ARG PIP3_INSTALL="python3 -m pip install --upgrade --no-cache-dir --target /usr/lib/python3/dist-packages"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install dependencies
RUN apt-get -qq update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y full-upgrade && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends \
      apt-transport-https \
      ca-certificates \
      gnupg \
      wget && \
    wget -qO- https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - && \
    if [ "${DISTRO}" = "bionic" ] || [ "${DISTRO}" = "xenial" ]; then \
        echo "deb https://apt.llvm.org/stretch/ llvm-toolchain-stretch-11 main" | tee /etc/apt/sources.list.d/llvm-11.list && \
        echo "deb https://apt.llvm.org/stretch/ llvm-toolchain-stretch-12 main" | tee /etc/apt/sources.list.d/llvm-12.list && \
        wget -qO- https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add - && \
        echo "deb https://apt.kitware.com/ubuntu/ ${DISTRO} main" | tee /etc/apt/sources.list.d/cmake.list && \
        wget -qO- 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x6A755776' | apt-key add - && \
        echo "deb http://ppa.launchpad.net/deadsnakes/ppa/ubuntu $DISTRO main " | tee /etc/apt/sources.list.d/deadsnakes.list ; \
    fi && \
    if [ "${DISTRO}" = "xenial" ]; then \
        echo "deb https://apt.llvm.org/${DISTRO}/ llvm-toolchain-${DISTRO}-9 main" | tee /etc/apt/sources.list.d/llvm-9.list && \
        echo "deb https://apt.llvm.org/xenial/ llvm-toolchain-xenial-11 main" | tee /etc/apt/sources.list.d/llvm-11.list && \
        echo "deb https://apt.llvm.org/xenial/ llvm-toolchain-xenial-12 main" | tee /etc/apt/sources.list.d/llvm-12.list ; \
    fi && \
    apt-get -qq update && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends \
      automake \
      bsdiff \
      build-essential \
      gawk \
      gdb \
      git \
      lcov \
      libarchive13 \
      libglib2.0-0 \
      liblzma-dev \
      libpython2.7 \
      libpython3-dev \
      libtool-bin \
      locales \
      pkg-config \
      procps \
      python3-pip \
      rsync \
      strace \
      sudo \
      tmux \
      vim \
      zlib1g-dev \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*  && \
    echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen && \
    locale-gen && \
    groupadd -g 1000 -r fuzz && \
    useradd -u 1000 --no-log-init -r -g fuzz fuzz && \
    mkdir -p /etc/sudoers.d && \
    echo "fuzz ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/fuzz && \
    chmod 0440 /etc/sudoers.d/fuzz && \
    ${PIP3_INSTALL} pip==20.3.4 && \
    ${PIP3_INSTALL} setuptools wheel && \
    ${PIP3_INSTALL} pyyaml requests

ENV LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    SETUPTOOLS_USE_DISTUTILS=stdlib

# RUN wget -qO /tmp/bsdiff.deb 'http://http.us.debian.org/debian/pool/main/b/bsdiff/bsdiff_4.3-21_i386.deb' && \
#     dpkg -i /tmp/bsdiff.deb && \
#     rm -f /tmp/bsdiff.deb
# COPY --from=registry.gitlab.com/rode0day/fuzzer-testing/dynamorio:16.04 \
#    /dynamorio/exports /opt/dr
