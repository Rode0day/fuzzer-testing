#!/bin/bash -e

unset REGISTRY
cp xenial.env .env

export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

if [ "$1" = "base" ]; then
    docker-compose build --parallel --no-cache base base_i386 || exit 1
    docker-compose push base_i386
    docker-compose build --parallel --no-cache builder builder_i386 || exit 1
    docker-compose push builder_i386
elif [ $# -gt 0 ]; then
    echo "$@"
    docker-compose build base
    docker-compose build builder
    docker-compose build ${1}_builder || echo NO builder
    docker-compose build $@ || exit 1
    docker-compose build --no-cache ${1}_runner || exit 1
    docker-compose push ${1}_runner
    exit 0
fi

docker-compose build --parallel base base_i386 || exit 1
docker-compose push base_i386
docker-compose build --parallel builder builder_i386 || exit 1
docker-compose push builder_i386

docker-compose build --parallel afl-rb wafl aflpp honggfuzz honggfuzz_v21 || exit 1
docker-compose build --parallel afl_i386 aflpp_i386 honggfuzz_i386 || exit 1
docker-compose push afl_i386 aflpp_i386 honggfuzz_i386
docker-compose build --parallel afl || exit 1
docker-compose build --parallel verify verify_i386 || exit 1
docker-compose push verify_i386
docker-compose build --parallel angora_builder eclipser_builder || exit 1
docker-compose build --parallel qsym eclipser angora intriguer ankou || exit 1

# build the runners
docker-compose build --parallel --no-cache afl_runner aflpp_runner || exit 1
docker-compose build --parallel --no-cache afl_i386_runner aflpp_i386_runner honggfuzz_i386_runner verify_i386_runner || exit 1
docker-compose build --parallel --no-cache qsym_runner honggfuzz_runner honggfuzz_v21_runner || exit 1
docker-compose build --parallel --no-cache eclipser_runner angora_runner ankou_runner intriguer_runner || exit 1
docker-compose build --parallel --no-cache test_runner verify_runner || exit 1

# these are tagged versions of built runners
docker-compose build aflrb_runner wafl_runner

# push to the repository
docker-compose push afl_runner aflpp_runner afl_i386_runner aflpp_i386_runner aflrb_runner qsym_runner \
                    honggfuzz_runner honggfuzz_i386_runner eclipser_runner angora_runner ankou_runner \
                    wafl_runner test_runner verify_runner verify_i386_runner honggfuzz_v21_runner \
