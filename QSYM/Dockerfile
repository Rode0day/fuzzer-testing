ARG BASE_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/base:16.04
ARG BUILDER_IMAGE=registry.gitlab.com/rode0day/fuzzer-testing/builder:16.04
ARG AFL_IMAGE="registry.gitlab.com/rode0day/fuzzer-testing/afl:16.04"
ARG LLVM_I386_IMAGE=builder_llvm:16.04
FROM $AFL_IMAGE as afl_image
FROM $LLVM_I386_IMAGE as llvm_i386
FROM $BUILDER_IMAGE as builder

RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      python-pip

RUN wget -qO /tmp/llvm.tar.xz https://releases.llvm.org/3.8.1/clang+llvm-3.8.1-x86_64-linux-gnu-ubuntu-16.04.tar.xz && \
    tar -C /usr --strip-components=1 -xf /tmp/llvm.tar.xz && \
    rm /tmp/llvm.tar.xz

RUN git clone --depth=1 -b 'z3-4.6.0' https://github.com/Z3Prover/z3.git && \
    cd z3 && \
    ./configure --prefix=/usr/local && \
    make -C build -j "$(nproc)" && \
    make -C build install && \
    rm -rf build && \
    ./configure --prefix=/usr/local --x86 && \
    make -C build -j "$(nproc)" && \
    cp build/libz3.so /usr/lib32/ && \
    cd / && rm -rf /z3

ARG CACHEBUST=ea4a7241
RUN git clone --depth=1 https://gitlab.com/wideglide/qsym /qsym && \
    git -C /qsym rev-parse HEAD | tee /usr/local/qsym_commit_hash && \
    cd  /qsym && \
#   sed -i 's,DCONFIG_CONTEXT_SENSITIVE,DCONFIG_CONTEXT_SENSITIVE -I/usr/lib/llvm-3.9/include,' qsym/pintool/makefile.rules && \
    python -m pip install wheel && \
    python -m pip install . && \
    cd / && rm -rf /qsym

COPY --from=afl_image /usr/local /usr/local


### STAGE 2 ###
FROM $BASE_IMAGE

RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      libstdc++6 \
      lib32stdc++6 \
      lib32gomp1 \
      libjpeg-turbo8 \
      python \
      python-pip \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

COPY --from=builder /usr/local /usr/local
COPY --from=builder /usr/lib32/libz3.so /usr/lib32/

ENV AFL_SKIP_CPUFREQ=1 \
    QEMU_RESERVED_VA=0xf700000 \
    FUZZER=qsym
WORKDIR /data
USER fuzz
ENTRYPOINT [ "/bin/bash" ]
