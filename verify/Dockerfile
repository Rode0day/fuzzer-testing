ARG BASE_IMAGE
ARG BUILDER_IMAGE
ARG AFL_IMAGE
FROM $AFL_IMAGE as afl_image
FROM $BUILDER_IMAGE as builder

ENV GOROOT=/opt/go \
    GOPATH=/root/go

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get -qq update && \
    wget -qO- https://dl.google.com/go/go1.14.linux-amd64.tar.gz | tar -C /opt -xz && \
    ln -s /opt/go/bin/go /usr/bin/ && \
    mkdir -p $GOPATH

RUN go get github.com/SRI-CSL/gllvm/cmd/... && \
    cp /root/go/bin/* /usr/local/bin/

COPY --from=afl_image /usr/local /usr/local


### STAGE 2 ###
FROM $BASE_IMAGE

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends \
      bison \
      flex \
      gdb \
      cmake \
      clang-9 \
      llvm-9-dev \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    ln -fLs /usr/lib/llvm-9/bin/clang /usr/bin/clang && \
    ln -fLs /usr/lib/llvm-9/bin/clang++ /usr/bin/clang++ && \
    ln -fLs /usr/lib/llvm-9/bin/llvm-config /usr/bin/llvm-config

COPY --from=builder /usr/local /usr/local
# COPY --from=registry.gitlab.com/rode0day/fuzzer-testing/dynamorio:16.04 \
#    /dynamorio/exports /opt/dr
RUN mkdir /opt/dr && \
    wget -qO- https://github.com/DynamoRIO/dynamorio/releases/download/release_8.0.0-1/DynamoRIO-Linux-8.0.0-1.tar.gz | \
    tar --strip=1 -C /opt/dr -xz && \
    find /opt/dr -type f -name '*.a' -delete && \
    rm -rf /opt/dr/docs /opt/dr/drmemory

ENV LLVM_CONFIG=llvm-config-9 \
    CC=gcc \
    CXX=g++ \
    CFLAGS="" \
    CXXFLAGS="" \
    AFL_SKIP_CPUFREQ=1 \
    QEMU_RESERVED_VA=0xf700000 \
    FUZZER=afl
WORKDIR /data
USER fuzz
ENTRYPOINT ["bash"]
