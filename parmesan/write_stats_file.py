#!/usr/bin/env python3

import json
import os
import sys
import time

stats_file = """start_time        : {start_time}
last_update       : {last_update}
fuzzer_pid        : {fuzzer_pid}
cycles_done       : {cycles_done}
execs_done        : {execs_done}
execs_per_sec     : {execs_per_sec}
paths_total       : {paths_total}
paths_favored     : 0
paths_found       : {paths_found}
paths_imported    : 0
max_depth         : 0
cur_path          : 0
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : 0.0
unique_crashes    : {unique_crashes}
unique_hangs      : {unique_hangs}
last_path         : {last_path}
last_crash        : {last_crash}
last_hang         : 0
execs_since_crash : 0
exec_timeout      : {exec_timeout}
afl_banner        : {banner}
afl_version       : v1.0.0
target_mode       : parmesan
command_line      : {cmdline}"""

if (len(sys.argv) != 2 or not os.path.isfile(sys.argv[1])):
    print("Usage: {} <path_to/angora/chart_stat.json>".format(sys.argv[0]))
    sys.exit(1)


def last_file(path):
    angora_root = os.path.dirname(sys.argv[1])
    angora_queue = os.path.join(angora_root, path)
    most_recent = max(os.scandir(angora_queue), key=lambda x: x.stat().st_mtime, default=None)
    if most_recent:
        return int(most_recent.stat().st_mtime)
    return 0


# Load chart_stat.json
j = json.load(open(sys.argv[1]))

now = int(time.time())
stats = dict()
env = os.environ

stats['start_time'] = env.get('START_TIME', now)
stats['last_update'] = now
stats['fuzzer_pid'] = env.get('FUZZER_PID', 0)
stats['cycles_done'] = j['num_rounds']
stats['execs_done'] = j['num_exec']
stats['execs_per_sec'] = j['speed'][0]
stats['paths_total'] = j['num_inputs']
stats['paths_found'] = j['num_inputs']
stats['unique_crashes'] = j['num_crashes']
stats['unique_hangs'] = j['num_hangs']
stats['last_path'] = last_file('queue')
stats['last_crash'] = last_file('crashes')
stats['exec_timeout'] = env.get('EXEC_TIMEOUT', 1)
stats['banner'] = "{}_parmesan".format(env.get('TGT', ""))
stats['cmdline'] = "{} {}".format(env.get('BINARY', ""), env.get('CMDLINE', ""))

print(stats_file.format(**stats))
