#!/bin/bash -ue

BIN_PATH="/parmesan/bin"
CC_BIN="${BIN_PATH}/angora-clang"
CXX_BIN="${BIN_PATH}/angora-clang++"
DIFF_BIN="${BIN_PATH}/llvm-diff-parmesan"
ID_ASSIGNER_PATH="${BIN_PATH}/pass/libLLVMIDAssigner.so"
PRUNE_SCRIPT_PATH="/parmesan/tools/prune.py"
FUZZER_PATH="${BIN_PATH}/fuzzer"

CFLAGS="${CFLAGS:-}"
CXXFLAGS="${CXXFLAGS:-}"
LDFLAGS="${LDFlAGS:--lz}"

log_run_cmd() {
    echo "[*] Executing: $@ "
    eval "$@"
}

build_pipeline() {
    local bc_file=$(basename -- "$1")
    local bc_path="$(dirname $1)"
    local name="${bc_file%.*}"
    local input_dir="${2:-}"
    local target_flags="${3:-@@} ${@:4}"
    local compiler="${CC_BIN}"
    local cflags="${CFLAGS:-}"
    local sanitizer="address"
    local targets_file="targets.json"

    if [ -d "${input_dir}" ]; then
        cp -r "${input_dir}" "${bc_path}"/seeds
    else
        mkdir seeds
        echo "ABCD" > seeds/empty
    fi

    pushd "$bc_path" >/dev/null
    
    #1) BUILD FAST LL
    log_run_cmd "USE_FAST=1 ${compiler} ${cflags} -S -emit-llvm -o ${name}.fast.ll ${bc_file}"
    #2) BUILD FAST BIN
    log_run_cmd "USE_FAST=1 ${compiler} ${cflags} -o ${name}.fast ${bc_file} ${LDFLAGS}"
    #3) BUILD FAST SAN LL
    log_run_cmd "USE_FAST=1 ${compiler} ${cflags} -fsanitize=${sanitizer} -S -emit-llvm -o ${name}.san.ll ${bc_file}"
    #4) BUILD FAST SAN BIN
    log_run_cmd "USE_FAST=1 ${compiler} ${cflags} -fsanitize=${sanitizer} -o ${name}.san.fast ${bc_file} ${LDFLAGS}"
    #5) BUILD TRACK BIN
    log_run_cmd "USE_TRACK=1 ${compiler} ${cflags} -o ${name}.track ${bc_file} ${LDFLAGS}"
    #6) Gather targets.json and target.diff
    log_run_cmd "${DIFF_BIN} -json ${name}.fast.ll ${name}.san.ll 2> ${name}.diff || echo returncode = \$?"
    #7) Gather cmp.map
    log_run_cmd "opt -load ${ID_ASSIGNER_PATH} -idassign -idassign-emit-cfg "\
                "-idassign-cfg-file cfg.dat ${name}.fast.ll >/dev/null"
    #8) Prune targets
    log_run_cmd "python3.8 ${PRUNE_SCRIPT_PATH} ${targets_file} ${name}.diff cmp.map "\
                "seeds ./${name}.track ${target_flags} > targets.pruned.json"
    
    rm -rf seeds
    rm -f profile_track.out
    popd >/dev/null

    # Print fuzzing command
    printf "You can now run your target application (with SanOpt enabled) using:\n\n"
    printf "${FUZZER_PATH} -c ./targets.pruned.json -i ${input_dir} -o out -t ./${name}.track -s ./${name}.san.fast -- ./${name}.fast ${target_flags}\n"
}


if [ $# -lt 1 ]; then
    printf "Usage: $0 BC_FILE SEEDS_DIR [ target program cmd args with @@ ]\n"
    printf "        Whwere BC_FILE is an llvm .bc file created by gclang / wllvm.\n"
    exit 1
fi

build_pipeline $1 "${2:-seeds}" "${@:3}"
