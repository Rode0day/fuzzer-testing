#!/bin/bash -eu

R_TAG="${1:-18.04}"
REGISTRY="registry.gitlab.com/rode0day/fuzzer-testing"

for f in \
afl \
i386/afl \
aflpp \
i386/aflpp \
angora \
eclipser \
honggfuzz \
i386/honggfuzz \
qsym 
do
    printf "%-20s :  " "$f"
    docker run --rm -it --entrypoint bash ${REGISTRY}/${f}:${R_TAG} -c "cat /usr/local/${f##*/}*commit_hash || cat /usr/local/*hash"
done

for f in \
eclipser_builder
do 
    printf "%-20s :  " "$f"
    docker run --rm -it --entrypoint bash ${f}:${R_TAG} -c "cat /usr/local/${f%%_*}*commit_hash"
done

