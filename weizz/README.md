# Weizz


**weizz usage**:

```
weizz 1.0a by <andreafioraldi@gmail.com>

weizz [ options ] -- /path/to/fuzzed_app [ ... ]

Required parameters:

  -i dir        - input directory with test cases
  -o dir        - output directory for fuzzer findings

Execution control settings:

  -f file       - location read by the fuzzed program (stdin)
  -t msec       - timeout for each run (auto-scaled, 50-1000 ms)
  -m megs       - memory limit for child process (50 MB)
  -L bytes      - size bounds to disable getdeps for a testcase
  -Q            - use binary-only instrumentation (QEMU mode)

Fuzzing behavior settings:

  -F            - full weizz mode, always perform surgical fuzzing
  -b            - force getdeps with bit flips, more accuracy
  -A            - aggressive mode, always enter in getdeps when pending_favs = 0
  -w            - smart mode, high-order mutate tagged inputs
  -h            - stacking mode, alternate smart and AFL mutations
  -l            - enable the locked havoc stage when surgical fuzzing
  -G            - after the getdeps stage stop fuzzing the current entry
  -c            - enable checksum patching
  -a            - avoid the get deps stage, almost like standard AFL
  -u            - disable the trim stage (use with uninformed inputs)
  -d            - quick & dirty mode (skips deterministic steps)
  -x dir        - optional fuzzer dictionary (see README)

Other stuff:

  -P patch_map  - load a checksums information map
  -T text       - text banner to show on the screen
  -M / -S id    - distributed mode (see parallel_fuzzing.txt)
  -e ext        - file extension for the temporarily generated test
  -C            - crash exploration mode (the peruvian rabbit thing)

For additional tips, please consult README.
```
