#!/bin/bash -u

docker image prune
docker rmi $( docker images  registry.gitlab.com/rode0day/fuzzer-testing/*:18.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images  registry.gitlab.com/rode0day/fuzzer-testing/*:20.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images  registry.gitlab.com/rode0day/fuzzer-testing/*:16.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images  registry.gitlab.com/rode0day/fuzzer-testing/*/*:16.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images  registry.gitlab.com/rode0day/fuzzer-testing/*/*:20.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images *_builder:18.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images *_builder:16.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images *_builder:20.04 --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images registry.gitlab.com/rode0day/fuzzer-testing/i386/*:* --format "{{.Repository}}:{{.Tag}}" )
docker rmi $( docker images registry.gitlab.com/rode0day/fuzzer-testing/*:* --format "{{.Repository}}:{{.Tag}}" )
exit 0
# docker rmi $( docker images registry.gitlab.com/fuzzing/magma/*:* --format "{{.Repository}}:{{.Tag}}" )
# docker rmi registry.gitlab.com/fuzzing/magma:18.04
docker rmi $( docker images *_builder:* --format "{{.Repository}}:{{.Tag}}" )
