#!/bin/bash


# ------------------------[  0 days 09 hrs 16 mins 45 secs ]----------------------
#   Iterations : 4,393,671 [4.39M]
#   Mode [3/3] : Feedback Driven Mode
#       Target : /usr/local/bin/qemu-i386 bin/sqlite
#      Threads : 4, CPUs: 16, CPU%: 805% [50%/CPU]
#        Speed : 120/sec [avg: 131]
#      Crashes : 0 [unique: 0, blacklist: 0, verified: 0]
#     Timeouts : 0 [10 sec]
#  Corpus Size : 2, max: 8,192 bytes, init: 2 files
#   Cov Update : 0 days 09 hrs 16 mins 45 secs ago
#     Coverage : edge: 0/0 [0%] pc: 635 cmp: 3,593
# ---------------------------------- [ LOGS ] ------------------/ honggfuzz 1.9 /-
parse() {
read _ _ iterations _
read _ _ cmdline
read _ _ threads _ cpus _
read _ _ speed _ avg 
read _ _ crashes _ c_unique _
read _ _ timeouts tmout _
read _ _ _ queue _ max_size _ _ init _
read _
read _ _ _ edge cov_pct _ cov_pc _ cov cmp

MODE=default
if [[ $cmdline == *'qemu'* ]]; then
    MODE=qemu
fi

if [ -z "$START_TIME" ]; then
    echo "[-] START_TIME not exported"
    START_TIME="$(stat -c "%Y" .jobvars)"
fi

last_path="$(stat -c "%Y" $(ls -t $Q_DIR | head -n1))"
last_crash="$(stat -c "%Y" $(ls -t $C_DIR | head -n1))"
banner="${TGT}_${FZ}"

echo "start_time        : $START_TIME
last_update       : $(date +"%s")
fuzzer_pid        : ${FPID:-0}
cycles_done       : 0
execs_done        : ${iterations//,/}
execs_per_sec     : ${avg}
paths_total       : ${queue}
paths_favored     : 0
paths_found       : $(( $queue - $init ))
paths_imported    : 0
max_depth         : 0
cur_path          : 0
pending_favs      : 0
pending_total     : 0
variable_paths    : 0
stability         : 100.00%
bitmap_cvg        : ${cov_pct}
unique_crashes    : ${c_unique}
unique_hangs      : ${timeouts}
last_path         : $last_path
last_crash        : $last_crash
last_hang         : 0
execs_since_crash : 0
exec_timeout      : $tmout
afl_banner        : ${banner}
afl_version       : v2.0
target_mode       : $MODE
command_line      : $cmdline"
}

head -n12 | sed '/----/d; /Mode/d; s/\x1B\[\([0-9;]\+\)m//g; s/,//g; s/\/sec//; s/\]//g; s/\[//g; /^\s*$/d; ' | parse

